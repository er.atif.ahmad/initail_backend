const express = require('express');
const createError = require('http-errors');
const sls = require('serverless-http');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
require('dotenv').config()

const indexRouter = require('./routes/index');

const knex = require('./db/knex');
const knexReader = require('./db/knex-reader');


const knexTestQuery = async () => {
  try {
    // do a test query to make sure the knex connection pool is ready to go after lambda unfreeze
    await knex.raw(`select 1 as "one";`);
    await knexReader.raw(`select 2 as "two";`);
    console.log("db connected");
  } catch (err) {
    // log but otherwise ignore errors here
    console.log('Postgres-Knex Revalidate Pool Query Error:', err.message);
  }
}





////////// App  /////////

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.enable('trust proxy');
app.set('trust proxy', 1);


app.use((req, res, next) => {
  res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  next();
});


app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, XMLHttpRequest, ngsw-bypass');
  next();
});


app.use(async (req, res, next) => {
  console.log('Validating Connection Pool of Knex...');
  await knexTestQuery();
  next();
});

app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  // res.locals.message = err.message;
  // res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  const error = res.locals.error = req.app.get('env') !== 'production' ? err : {};

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, XMLHttpRequest, ngsw-bypass');

  // render the error page
  res.status(err.status || 500);
  res.render(error);
});

module.exports = app;


if (!process.env.VAPID_PUBLIC_KEY || !process.env.VAPID_PRIVATE_KEY) {
  console.log(`You must set the VAPID_PUBLIC_KEY and VAPID_PRIVATE_KEY environment variables to enable push notification.`);
  // console.log(webPush.generateVAPIDKeys());
}



global.appRoot = path.resolve(__dirname);


module.exports.s3hook = (event, context) => {
  console.log('S3 Hook: Event   :: ', JSON.stringify(event));
  console.log('S3 Hook: Context ::', JSON.stringify(context));
  // console.log(JSON.stringify(process.env));
};

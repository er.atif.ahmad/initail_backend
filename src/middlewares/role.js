const createError = require("http-errors");
const permissionHelper = require('../helpers/permission');

const roleMiddleware = {

  parseUserPermission: async (req, res, next) => {
    try {
      if (!req.me) {
        next(createError(401));
      }

      req.accessibleResources = await permissionHelper.getUserAccessibleResources(req.me);
      console.log(`'[middleware][role][parseUserPermission] accessibleResources:`, req.accessibleResources);

      next();

    } catch (err) {
      console.error(`'[middleware][role][parseUserPermission] Error:`, err);
      return res.status(500).json({ error: err });
    }
  }
};

module.exports = roleMiddleware;

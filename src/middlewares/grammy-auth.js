const knex = require('../db/knex');
const createError = require('http-errors');
var jwt = require('jsonwebtoken');
const redisHelper = require('../helpers/redis');


const grammyAuthMiddleware = {

    isAuthenticated: async (req, res, next) => {

        try {

            if (!req.headers || !req.headers.authorization) {
                next(createError(401));
            }

            let token = req.headers.authorization;
            token = token.replace('Bearer ', '');

            let currentUser = {
                id : 999999999,
                name: 'GRAMMY_EXPOSED_ENDPOINT_USER',
                userName: 'GRAMMY_EXPOSED_ENDPOINT_USER',
                email:'',
                password: 'sdas4342vcxfd@454$$%q23432gffhtyedsdfgfdgfdg5434#ewre',
            };

            if (token && token != '') {

                // Very token using JWT
                console.log('[grammyAuthMiddleware][isAuthenticated]: Token Provided:', token);

                if(!(token == currentUser.password)) {
                    return next(createError(401));
                } 

                req.id = currentUser.id;
            
                req.me = currentUser;

                console.log('[grammyAuthMiddleware][isAuthenticated]: Current User:', currentUser.id, currentUser.email);
                return next();

            } else {
                return next(createError(401));
            }
        } catch (err) {
            console.log('[grammyAuthMiddleware][isAuthenticated]:  Error', err);
            next(createError(401));
        }
    },

};

module.exports = grammyAuthMiddleware;



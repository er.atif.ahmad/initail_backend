const knex = require('../db/knex');
const knexReader = require('../db/knex-reader');
const createError = require('http-errors');
var jwt = require('jsonwebtoken');
// const redisHelper = require('../helpers/redis');


const authMiddleware = {

    isAuthenticated: async (req, res, next) => {

        try {

            if (!req.headers || !req.headers.authorization) {
                next(createError(401));
            }

            let token = req.headers.authorization;
            token = token.replace('Bearer ', '');

            if (token && token != '') {

                // Very token using JWT
                console.log('[isAuthenticated]: Token', token);
                console.log('[process.env]: JWT_PRIVATE_KEY', process.env.JWT_PRIVATE_KEY);
                const decodedTokenData = await jwt.verify(token, process.env.JWT_PRIVATE_KEY);
                // console.log('[middleware][auth]: Token Decoded Data:', decodedTokenData);

                req.id = decodedTokenData.id;

                let currentUser = await knexReader('shops').where({ id: decodedTokenData.id }).first();

                // console.log('[middleware][auth]: Current User:', currentUser);

                if (currentUser.isActive) {

                    // Trying to get application role from cache..
                    // const key = `user_application_roles-${currentUser.id}`;
                    // let userApplicationRole = await redisHelper.getValue(key);
                    userApplicationRole = await knexReader('application_roles').where({ id: Number(currentUser.roleId) }).select('name').first();
                    console.log(`[middleware][auth][isAuthenticated]: From userApplicationRole:`, userApplicationRole);
                    // if (!userApplicationRole) {
                    //     // An user can have atmost one application role
                    //     userApplicationRole = await knexReader('application_roles').where({ id: Number(currentUser.roleId) }).select('name').first();
                    //     // await redisHelper.setValueWithExpiry(key, userApplicationRole, 120);
                    // }

                    currentUser.roles = userApplicationRole;

                    // switch (Number(userApplicationRole.roleId)) {
                    //     case 1:
                    //         currentUser.isAdmin = true;
                    //         currentUser.roles = ['admin'];
                    //         break;
                    //     case 2:
                    //         currentUser.isStaff = true;
                    //         currentUser.roles = ['staff'];
                    //         break;      
                    // }

                    delete currentUser.password;
                    delete currentUser.verifyToken;
                    delete currentUser.verifyTokenExpiryTime;
                    delete currentUser.emailVerified;
                    
                    req.me = currentUser;

                    console.log('[middleware][auth]: Current User:', currentUser.id, currentUser.email);
                    return next();
                }

                // If currentUser isNotActive
                return next(createError(401));

            } else {
                return next(createError(401));
            }
        } catch (err) {
            console.log('[middleware][auth] :  Error', err);
            next(createError(401));
        }
    },

    isAdmin: async (req, res, next) => {
        try {

            let currentUser = req.me;

            if (currentUser.roles && currentUser.roles.some(e => e === 'admin')) {
                return next();
            }

            return next(createError(403));
        } catch (err) {
            console.log('[middleware][auth][isSuperAdmin] :  Error', err);
            next(createError(401));
        }
    },

    isStaff: async (req, res, next) => {
        try {

            let currentUser = req.me;
            if (currentUser.roles && currentUser.roles.some(e => e === 'staff')) {
                return next();
            }

            return next(createError(403));
        } catch (err) {
            console.log('[middleware][auth][isOrgAdmin] :  Error', err);
            next(createError(401));
        }

    },
};

module.exports = authMiddleware;



const createError = require('http-errors');
const _ = require('lodash');


const resourceAccessMiddleware = {

  isAccessControlAccessible: async (req, res, next) => {
    if (req.me.isAdmin) {
      return next();
    }
    let permissions = [];
    // Get All Permissions....
    let requiredKeyObjs = req.accessibleResources.filter(ar => ar.resourceKey == 'accessControl');
    requiredKeyObjs.forEach(o => {
      permissions.push(...o.permissions);
    });
    if (permissions.length) {
      console.log('[middleware][resourceAccessMiddleware]: isAccessControlAccessible: Permissions:', permissions);
      req.permissions = permissions;
      return next();
    } else {
      console.log('[middleware][resourceAccessMiddleware]: isAccessControlAccessible: ', false);
      return next(createError(403));
    }
  },

  isMasterSetupAccessible: async (req, res, next) => {
    if (req.me.isAdmin) {
      return next();
    }
    let permissions = [];
    // Get All Permissions....
    let requiredKeyObjs = req.accessibleResources.filter(ar => ar.resourceKey == 'masterSetup');
    requiredKeyObjs.forEach(o => {
      permissions.push(...o.permissions);
    });
    if (permissions.length) {
      console.log('[middleware][resourceAccessMiddleware]: isMasterSetupAccessible: Permissions:', permissions);
      req.permissions = permissions;
      return next();
    } else {
      console.log('[middleware][resourceAccessMiddleware]: isMasterSetupAccessible: ', false);
      return next(createError(403));
    }
  },

  isServiceSetupAccessible: async (req, res, next) => {
    if (req.me.isAdmin) {
      return next();
    }
    let permissions = [];
    // Get All Permissions....
    let requiredKeyObjs = req.accessibleResources.filter(ar => ar.resourceKey == 'serviceSetup');
    requiredKeyObjs.forEach(o => {
      permissions.push(...o.permissions);
    });
    if (permissions.length) {
      console.log('[middleware][resourceAccessMiddleware]: isServiceSetupAccessible: Permissions:', permissions);
      req.permissions = permissions;
      return next();
    } else {
      console.log('[middleware][resourceAccessMiddleware]: isServiceSetupAccessible: ', false);
      return next(createError(403));
    }
  },

  isDowntimeSetupAccessible: async (req, res, next) => {
    if (req.me.isAdmin) {
      return next();
    }
    let permissions = [];
    // Get All Permissions....
    let requiredKeyObjs = req.accessibleResources.filter(ar => ar.resourceKey == 'downtimeSetup');
    requiredKeyObjs.forEach(o => {
      permissions.push(...o.permissions);
    });
    if (permissions.length) {
      console.log('[middleware][resourceAccessMiddleware]: isDowntimeSetupAccessible: Permissions:', permissions);
      req.permissions = permissions;
      return next();
    } else {
      console.log('[middleware][resourceAccessMiddleware]: isDowntimeSetupAccessible: ', false);
      return next(createError(403));
    }
  },

  isBroadcastMonitoringAccessible: async (req, res, next) => {
    if (req.me.isAdmin) {
      return next();
    }
    let permissions = [];
    // Get All Permissions....
    let requiredKeyObjs = req.accessibleResources.filter(ar => ar.resourceKey == 'broadcastMonitoring');
    requiredKeyObjs.forEach(o => {
      permissions.push(...o.permissions);
    });
    if (permissions.length) {
      console.log('[middleware][resourceAccessMiddleware]: isBroadcastMonitoringAccessible: Permissions:', permissions);
      req.permissions = permissions;
      return next();
    } else {
      console.log('[middleware][resourceAccessMiddleware]: isBroadcastMonitoringAccessible: ', false);
      return next(createError(403));
    }
  },

  isContentSchedulingAccessible: async (req, res, next) => {
    if (req.me.isAdmin) {
      return next();
    }
    let permissions = [];
    // Get All Permissions....
    let requiredKeyObjs = req.accessibleResources.filter(ar => ar.resourceKey == 'contentScheduling');
    requiredKeyObjs.forEach(o => {
      permissions.push(...o.permissions);
    });
    if (permissions.length) {
      console.log('[middleware][resourceAccessMiddleware]: isContentSchedulingAccessible: Permissions:', permissions);
      req.permissions = permissions;
      return next();
    } else {
      console.log('[middleware][resourceAccessMiddleware]: isContentSchedulingAccessible: ', false);
      return next(createError(403));
    }
  },

};

module.exports = resourceAccessMiddleware;
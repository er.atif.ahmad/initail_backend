const _ = require('lodash');
const knex = require('../db/knex');
const { v4: uuidv4 } = require('uuid');
const AWS = require("aws-sdk");

const imageHelper = {

    getUploadURL: async (mimeType, fileName, type = "") => {

        let re = /(?:\.([^.]+))?$/;
        let ext = re.exec(fileName)[1];
        let uploadFolder = type + "/";
        const actionId = uuidv4();
        const s3Params = {
            Bucket: process.env.S3_BUCKET_NAME,
            Key: `${uploadFolder}${actionId}.${ext}`,
            ContentType: mimeType,
            ACL: "public-read"
        };
        return new Promise(async (resolve, reject) => {
            const s3 = new AWS.S3({
                'signatureVersion':'v4'
            });
            let uploadURL = await s3.getSignedUrl("putObject", s3Params);
            // if (process.env.IS_OFFLINE) {
            //     uploadURL = uploadURL
            //         .replace("https://", "http://")
            //         .replace(".com", ".com:8000");
            // }
        
            resolve({
                // isBase64Encoded: false,
                // headers: { "Access-Control-Allow-Origin": "*" },
                uploadURL: uploadURL,
                generatedFilename: `${actionId}.${ext}`,
                fileName: fileName,
                uploadPath: "/" + s3Params.Key,
                resourceUrl: `${process.env.S3_BUCKET_URL}/${uploadFolder}${actionId}.${ext}`
            });
        });

    },

    getImage: async (entity_id, entity_type) => {
        try {

            const bucketUrl = process.env.S3_BUCKET_URL;

            let image = await knex('images').where({ entity_id, entity_type }).orderBy('id', 'desc').select('*').first();
            if (image) {
                return { title: image.title, fileName: image.file_name, s3Url: bucketUrl + '/'+ image.upload_path };
            } else {
                return null;
            }

        } catch (err) {
            console.log('[helpers][image][getImage]:  Error', err);
            return { code: 'UNKNOWN_ERROR', message: err.message, error: err };
        }
    }

};

module.exports = imageHelper;
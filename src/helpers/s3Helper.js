const AWS = require('aws-sdk');
const fs = require('fs-extra');


const s3Uploader = {
    uploadLocalFile: (localFilePath, destinationFilePath, s3BucketName) => {

        const bucketName = s3BucketName || process.env.S3_BUCKET_NAME;
        console.log("[helpers][uploadLocalFile]: Bucket Name :", bucketName);

        const s3 = new AWS.S3();

        return new Promise(async (resolve, reject) => {

            try {
                const uploadBuffer = await fs.readFile(localFilePath, 'utf8');

                const params = {
                    Bucket: bucketName,
                    Key: destinationFilePath,
                    Body: uploadBuffer,
                    ACL: "public-read"
                };

                let s3Res = await s3.putObject(params).promise();
                console.log("[helpers][uploadLocalFile]: File uploaded Successfully on s3...", s3Res);
                resolve(s3Res);
            } catch (err) {
                reject(err);
            }
        });
    },

    downloadFileToLocal: (s3FilePath, localFilePath, s3BucketName) => {

        const bucketName =  s3BucketName || process.env.S3_BUCKET_NAME;
        console.log("[helpers][downloadFileToLocal]: Bucket Name :", bucketName);

        return new Promise(async (resolve, reject) => {

            try {

                const params = {
                    Bucket: bucketName,
                    Key: s3FilePath,
                };

                const s3 = new AWS.S3({
                    'signatureVersion': 'v4'
                });

                const f = await s3.getObject(params).promise();
                await fs.ensureFile(localFilePath);
                await fs.writeFile(localFilePath, f.Body);
                console.log("[helpers][downloadFileToLocal]: File Downloaded & Saved Locally:", localFilePath);
                resolve(localFilePath);

            } catch (err) {
                reject(err);
            }
        });
    }
}



module.exports = s3Uploader;
const redisHelper = require('./redis');
const knex = require("../db/knex");
const knexReader = require("../db/knex-reader");


const permissionHelper = {
    getUserAccessibleResources: async (user) => {

      console.log('[helpers][permission][getUserAccessibleResources]:', user.id);
      let userAccessibleResources = [];
      
      let redisKey = `userAccessibleResources-${user.id}`;

      if (user.isAdmin) {
        // Get List of all permissions and give to the user...
        console.log('[helpers][permission][getUserAccessibleResources]: Current User is Admin');

        userAccessibleResources = await knexReader('resources as r')
          .select(['r.id as resourceId', 'r.resourceKey']);

        userAccessibleResources = userAccessibleResources.map(uar => {
            uar.permissions = ["*"];
            return uar;
        })  

      }

      if (user.isStaff) {
        console.log('[helpers][permission][getUserAccessibleResources]: Current User is Staff');
        const userRoles = await knexReader('team_users as tu')
          .innerJoin('teams as t', 't.teamId', 'tu.teamId')
          .innerJoin('team_roles_master as trm', 'trm.teamId', 'tu.teamId')
          .innerJoin('application_roles as ar', 'ar.id', 'trm.roleId')
          .select([
            'tu.teamId', 'tu.userId', 't.teamName',
            'trm.roleId', 'ar.name as roleName'
          ]).where({
            'tu.userId': user.id, 
            't.isActive': true,
            'trm.isActive': true
          });

        const userRoleIds = userRoles.map((role) => role.roleId);
        console.log('[helpers][permission][getUserAccessibleResources]: User Role Ids', userRoleIds);

        userAccessibleResources = await knexReader('role_resource_access_master as rram')
          .innerJoin('resources as r', 'r.id', 'rram.resourceId')
          .select(['rram.resourceId', knex.raw('jsonb_array_elements(jsonb_agg("accessType")) as permissions'), 'r.resourceKey'])
          .whereIn('rram.roleId', userRoleIds)
          .groupBy('rram.resourceId')
          .groupBy('r.resourceKey');
       
        console.log('[helpers][permission][getUserAccessibleResources]: userAccessibleResources: ', userAccessibleResources);

        await redisHelper.setValueWithExpiry(redisKey, userAccessibleResources, 180);

      }

      return userAccessibleResources;

    },
}



module.exports = permissionHelper;
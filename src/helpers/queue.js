const AWS = require('aws-sdk');

const sendToSQS = async (payload, messageAttributes, delay = 0) => {
    // console.log('[helpers][sendToSQS]:', JSON.stringify(payload, null, 4));

    let sqsUrl = process.env.SQS_URL;
    switch (payload.operatorId) {
        case 1:
            sqsUrl = process.env.AIS_SQS_URL;
            break;
        case 2:
            sqsUrl = process.env.TRUEH_SQS_URL;
            break;
        case 3:
            sqsUrl = process.env.DTAC_SQS_URL;
            break;
        case 4:
            sqsUrl = process.env.CAT_SQS_URL;
            break;
    }

    console.log('[helpers][sendToSQS]: Selected SQS URL', sqsUrl);

    const createdAt = new Date().toISOString();
    
    if (!messageAttributes) {
        messageAttributes = {
            "createdAt": {
                DataType: "String",
                StringValue: createdAt
            },
            "messageType": {
                DataType: "String",
                StringValue: 'UNDEFINED'
            }
        };
    }

    let params = {
        DelaySeconds: delay,
        MessageAttributes: messageAttributes,
        MessageBody: JSON.stringify(payload),
        QueueUrl: sqsUrl
    };

    return new Promise(async (resolve, reject) => {
        const sqs = new AWS.SQS({ apiVersion: '2012-11-05' });
        sqs.sendMessage(params, (err, data) => {
            if (err) {
                console.log("SQS Message POST Error:", err);
                reject(err)
            } else {
                console.log(`SQS Message POST Success On Queue( ${sqsUrl}):`, data.MessageId);
                resolve(data);
            }
        });
    });

}

module.exports = sendToSQS;
var parseString = require('xml2js').parseString;

const normalizeObject = (payload) => {

    // console.log('----------->Payload Recieved:', payload);

    let newObj = {};
    Object.keys(payload).forEach(key => {
        // console.log('For Key:', key, typeof payload[key])
        if(!key.startsWith('xmlns') && !key.startsWith('xsi:schemaLocation')) {
            if (typeof payload[key] === 'object' && payload[key] !== null) {
                if (key == '$' || key == '0' || key == 0 || key == 'number') {
                    newObj = { ...payload[key], ...newObj };
                } else {
                    newObj[key] = normalizeObject(payload[key]);
                }
            } else {
    
                if (key == 0 && typeof payload === 'object' && Object.keys(payload).length == 1) {
                    newObj = payload[key];
                } else {
                    newObj[key] = payload[key];
                }
            }   
        }
    });
    return newObj;
};

const depthOf = (object) => {
    var level = 1;
    for (var key in object) {
        if (!object.hasOwnProperty(key)) continue;

        if (typeof object[key] == 'object') {
            var depth = depthOf(object[key]) + 1;
            level = Math.max(depth, level);
        }
    }
    return level;
}

const utilityHelper = {

    processXMLPayload: async (payload) => {
        let newObj = normalizeObject(payload);

        let depth = depthOf(newObj);
        console.log('Object Depth:', depth);

        for (let i = 0; i < (depth / 2) + 1; i++) {
            // console.log('===========> Normalizing Obj (Before):', i, newObj)
            newObj = normalizeObject(newObj);
            // console.log('===========> Normalizing Obj: (After)', i, newObj)
        }
        return newObj;
    },

    parseXMLFromString: async (payload) => {
        return new Promise((resolve, reject) => {
            parseString(payload, (err, result) => {
                if(err) return reject(err);
                resolve(result);
            });
        });
    },

    csvToArray: (strData, strDelimiter) => {
        strDelimiter = strDelimiter || ',';

        const objPattern = new RegExp(
            '(\\' +
            strDelimiter +
            '|\\r?\\n|\\r|^)' +
            '(?:"([^"]*(?:""[^"]*)*)"|' +
            '([^"\\' +
            strDelimiter +
            '\\r\\n]*))',
            'gi'
        );

        const arrData = [[]];
        let arrMatches = null;

        while ((arrMatches = objPattern.exec(strData))) {
            const strMatchedDelimiter = arrMatches[1];
            if (strMatchedDelimiter.length && strMatchedDelimiter != strDelimiter) {
                arrData.push([]);
            }
            let strMatchedValue = '';
            if (arrMatches[2]) {
                strMatchedValue = arrMatches[2].replace(new RegExp('""', 'g'), '"');
            } else {
                strMatchedValue = arrMatches[3];
            }
            arrData[arrData.length - 1].push(strMatchedValue);
        }
        // Return the parsed data.
        return arrData;
    }

}



module.exports = utilityHelper;
const knex = require('../../../db/knex');
const respondToOperator = require('./respond-to-operator');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const broadcastIstMt = require('./ist-mt-broadcast');


const processSMSRequest = async (payload) => {

    try {

        let { requestRawBody, responseRawBody } = payload;
        delete payload.requestRawBody;
        delete payload.responseRawBody;

        console.log('[helpers][operator][cat][sms]:  PAyload:', JSON.stringify(payload));

        // Try to Register...
        let queryData = {
            query_name: "cat_register_service_sms",
            operator_rid: payload.operatorId,
            channel_rid: payload.channelId,
            partner_rid: payload.operatorId,            // Same as operator rid
            destination_address_number: payload.request.msg.destination,
            ud: payload.commandWord,
            source_address_number: payload.request.msg.originator,      // Should be MSISDN
            original_message_json: payload,
            ussd_code: payload.ussdCode,
            short_code: payload.shortCode,
            service_id: payload.serviceId,
            txid: payload.request.txid,
            ticketid: payload.ticketId,
            chargetype: payload.request.charge.chargetype
        };

        console.log(`[helpers][operator][cat][sms]: Prepared Query Data (SMS):`, JSON.stringify(queryData));


        let data = await execDbProcedure(queryData);
        // console.log('[helpers][operator][cat][sms]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][cat][sms]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][cat][sms]: Return Value:', JSON.stringify(data));

        const url = data.request_mt_notification.request_register_ip;                   // Coming From Response Endpoint Setting

        console.log('[helpers][operator][cat][sms]: Operator Endpoint (Ip):', url);
        console.log('[helpers][operator][cat][sms]: Response Type From DB Call:', data.response_type);

        if (url) {
            if (data.response_type == 'response_on_register') {

                await respondToOperator(data, url);
                await broadcastIstMt(data.first_mt_info);

            } else if (data.response_type == 'duplicate_registration') {

                await respondToOperator(data, url);

            } else if (data.response_type == 'wrong_format') {

                await respondToOperator(data, url);

            } else if (data.response_type == 'response_on_cancel' || 'duplicate_cancellation') {

                await respondToOperator(data, url);

            } else {
                console.log('[helpers][operator][cat][sms]: Unknow Response Type From DB Call:', data.response_type);
            }

        } else {
            console.warn(`[helpers][operator][cat][sms]: Operator Endpoint is null, Hence Aborting, Please check setup`);
        }


        // Save Incoming Request To Log...
        queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: data.registration_transaction_id,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "I",
            request_json: {
                ...payload,
                requestRawBody: requestRawBody
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                ...data,
                responseRawBody: responseRawBody
            },
            response_timestamp: new Date().toISOString(),
            message_info: ""   // notify_user or notify_operator In case of outgoing
        };

        console.log(`[helpers][operator][true][sms]: Saving Incoming Request Log:`, queryData);
        await execDbProcedure(queryData);

        return data;

    } catch (err) {
        console.error('[helpers][operator][cat][sms]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processSMSRequest;
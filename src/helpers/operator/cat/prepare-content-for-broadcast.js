const axios = require('axios').default;
const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);

const prepareXMLContentPayloadForSMS = async ({ username, password }, smsText) => {

    return `
    <?xml version="1.0" encoding="UTF-8" ?> 
    <request>
        <txid>TS_YYYYMMDDHHmmssSSS_TS</txid>
        <user>${username}</user>
        <password>${password}</password>
        <charge>
            <chargetype>CAT_CHARGE_TYPE</chargetype>
            <ticketid>CAT_TICKET_ID</ticketid>
        </charge>
        <msg>
            <destination>66_MSISDN_99</destination>
            <message><![CDATA[${smsText}]]></message>
            <langid>T</langid>
            <timestamp>${new Date().getTime()}</timestamp>
            <option></option>
        </msg>
    </request>
`;
};



const prepareContentForBroadcast = async (roundData, operatorData) => {

    let contentTypeCode = roundData.content_settings && roundData.content_settings.code ? roundData.content_settings.code : null;
    let smsText;
    let xml;
    let unicodedResponse;


    console.log('Content Type for given content_settings for round id:', contentTypeCode, roundData.bc_round_rid);

    switch (contentTypeCode) {
        case 'txt70':
        case 'txt140':
            smsText = roundData.content_value_json.text_content;
            break;
        case 'txt140_rtf_lnk':
        case 'pick_a_card_4':    
        case 'pick_a_card_6':    
            smsText = roundData.content_value_json.sms_message + ' ' + roundData.content_value_json.url;
            break;
        default:
            console.warn('No Content Type Matched for given content settings for round id:', roundData.bc_round_rid);
    }

    console.log('[helpers][operator][dtac][prepare-content-for-broadcast]: SMS To Be Broadcasted:', smsText);
    xml = await prepareXMLContentPayloadForSMS(operatorData, smsText);
    console.log('[helpers][operator][dtac][prepare-content-for-broadcast]: Content:', xml);
    return xml;

};

module.exports = prepareContentForBroadcast;
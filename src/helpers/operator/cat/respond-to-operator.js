const axios = require('../../../helpers/axios');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');


const respondToOperator = async (data, url) => {

    const xml = `
        <?xml version="1.0" encoding="UTF-8" ?> 
        <request>
            <txid>${data.txid_new}</txid>
            <user>${data.request_mt_notification.request_username}</user>
            <password>${data.request_mt_notification.request_password}</password>
            <charge>
                <chargetype>${data.chargetype}</chargetype>
                <ticketid>${data.ticketid}</ticketid>
            </charge>
            <msg>
                <destination>${data.member_info.msisdn}</destination>
                <message><![CDATA[${data.request_mt_notification.request_text}]]></message>
                <langid>T</langid>
                <timestamp>${new Date().getTime()}</timestamp>
                <option></option>
            </msg>
        </request>
    `;

    console.log('[helpers][operator][cat][respond-to-operator]: URL:', url);
    console.log('[helpers][operator][cat][respond-to-operator]: Sending Subscription Message Body:', xml);


    // Get the Operator Ip Address for sending these XML Messages...
    // For Staging ==> 52.77.249.254
    let axiosResponse = await axios.post(
        url,
        xml,
        {
            headers: {
                'Content-Type': 'text/xml'
            }
        }
    );

    if (axiosResponse.status == 200) {
        console.log('[helpers][operator][cat][respond-to-operator]: Send Operation Status to Operator Successfully:', axiosResponse.data);

        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xml
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                responseRawBody: axiosResponse.data
            },
            response_timestamp: new Date().toISOString(),
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][true][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);


    } else {
        console.error('[helpers][operator][cat][respond-to-operator]: Failed to Send Operation Status to Operator:', axiosResponse);
        
        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xml
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                responseRawBody: axiosResponse
            },
            response_timestamp: new Date().toISOString(),
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][true][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);
        
        throw new Error(`Failed to notify Operator about operation status: ${axiosResponse}`);
    }

};

module.exports = respondToOperator;
const knex = require('../../../db/knex');
const axios = require('../../../helpers/axios');
const utilityHelper = require('../../../helpers/utility');
const execDbProcedure = require('../../db/exec-db-procedure');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);

const initiateCallCenterRequestProcess = async (payload) => {

    try {

        console.log('[helpers][operator][cat][call-center-initiate-process]:  Payload:', JSON.stringify(payload));

        // Try to Register...
        let queryData = {
            query_name: "cat_get_callcenter_channel_info",
            operator_rid: payload.operatorId,
            channel_rid: payload.channelId,
            partner_rid: payload.operatorId,            // Same as operator rid
            service_id: payload.message.cc['service-id'],
            short_code: payload.message.cc['service-id'],
            msisdn: payload.message.cc.source.address._,
        };

        console.log(`[helpers][operator][cat][call-center-initiate-process]: Prepared Query Data (SMS):`, JSON.stringify(queryData));


        let data = await execDbProcedure(queryData);
        // console.log('[helpers][operator][cat][call-center-initiate-process]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][cat][call-center-initiate-process]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][cat][call-center-initiate-process]: Return Value:', JSON.stringify(data));



        let timestamp = moment().format('YYYYMMDDHHmmssSSS');

        let messageKeyword = data.request_register_ud;
        let url = `http://isag.cat3g.com/api/api.cp.whitelist.php`;

        switch (payload.message.cc.ud._) {
            case 'R':
                messageKeyword = data.request_register_ud;
                url = data.request_register_endpoint.ip_address;
                break;
            case 'C':
                messageKeyword = data.request_cancel_ud;
                url = data.request_cancel_endpoint.ip_address;
                break;
        }

        let xml = `
    <?xml version="1.0" encoding="UTF-8" ?>
    <request>
        <txid>${data.txid}</txid>
        <chargetype>${data.chargetype}</chargetype>
        <originator>${data.msisdn}</originator>
        <msgtype>${data.msgtype}</msgtype>
        <message>${messageKeyword}</message>
        <timestamp>${timestamp}</timestamp>
    </request>
    `;


        console.log('[helpers][operator][cat][call-center-initiate-process]: callCenterRegisterInitiate Notification URL:', url);
        console.log('[helpers][operator][cat][call-center-initiate-process]: callCenterRegister Notification XML:', xml);


        try {
            let axiosResponse = await axios.post(
                url,
                xml,
                {
                    headers: {
                        'Content-Type': 'application/xml',
                    }
                }
            );

            console.log('[helpers][operator][cat][call-center-initiate-process]: Success Response:', axiosResponse.data);
            let parsedJson = await utilityHelper.parseXMLFromString(axiosResponse.data);
            let simplifiedJson = await utilityHelper.processXMLPayload(parsedJson);


            queryData = {
                query_name: "member_activity_log_req_res_save",
                record_id: null,
                member_activity_log_rid: null,
                registration_transaction_id: data.txid,
                in_out: "O",
                request_json: {
                    ...data,
                    requestRawBody: xml
                },
                request_timestamp: new Date().toISOString(),
                response_json: {
                    simplifiedJson: simplifiedJson,
                    responseRawBody: axiosResponse.data
                },
                response_timestamp: new Date().toISOString(),
                message_info: "notify_operator"   // notify_user or notify_operator In case of outgoing
            };

            console.log(`[helpers][operator][cat][call-center-initiate-process]: Saving Incoming Request Log:`, queryData);
            await execDbProcedure(queryData);


        } catch (err) {
            if (err.response) {
                // client received an error response (5xx, 4xx)
                console.error('[helpers][operator][cat][call-center-initiate-process]: Failed to Send Message to Operator: Recieved Error Response:', err.message);
            } else if (err.request) {
                // client never received a response, or request never left
                console.error('[helpers][operator][cat][call-center-initiate-process]: Failed to Send Message to Operator: No Response:', err.message);
            } else {
                // anything else
                console.error('[helpers][operator][cat][call-center-initiate-process]: Some other Error in Sending Message to Operator:', err);
            }


            let queryData = {
                query_name: "member_activity_log_req_res_save",
                record_id: null,
                member_activity_log_rid: null,
                registration_transaction_id: data.txid,
                in_out: "O",
                request_json: {
                    ...data,
                    requestRawBody: xml
                },
                request_timestamp: new Date().toISOString(),
                response_json: {
                    simplifiedJson: JSON.stringify(err.message),
                    responseRawBody: err
                },
                response_timestamp: new Date().toISOString(),
                operator_status_code: null,
                operator_status_description: 'Network Error',
                message_info: "notify_operator"   // notify_user or notify_operator
            };

            console.log(`[helpers][operator][dtac][respond-to-operator]: Saving Response Data:`, queryData);
            await execDbProcedure(queryData);

            throw new Error(`Failed to trasnfer call center action to operator: ${err.message}`);

        }


    } catch (err) {
        console.error('[helpers][operator][cat][call-center-initiate-process]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = initiateCallCenterRequestProcess;
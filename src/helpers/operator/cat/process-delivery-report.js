const axios = require('../../../helpers/axios');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const utilityHelper = require('../../../helpers/utility');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const processDeliveryReport = async (payload) => {

    try {

        console.log(`[helpers][cat][process-delivery-report]: Message:`, JSON.stringify(payload));

        // Now call db procedure calls to log...

        let queryData = {
            query_name: "broadcast_content_rounds_service_member_log_save",
            record_id: null,
            server_log_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
            incoming_outgoing_flag: "I",
            request_message: payload,
            response_message: null,
            "internal_message_id": null,
            "external_message_id": payload.request.dr.smstxid,
            "message_type": 'DR',
            "message_status": payload.request.dr.status,
            "message_code": null,
            "message_description": payload.request.dr.description,
            "operator_rid": payload.operatorId
        };
        const data = await execDbProcedure(queryData);

        console.log(`[helpers][cat][process-delivery-report]: DR Update data:`, JSON.stringify(data));

    } catch (err) {

        console.error('[helpers][cat][process-delivery-report]: Unable to process:', err);
        throw err;
    }
};

module.exports = processDeliveryReport;
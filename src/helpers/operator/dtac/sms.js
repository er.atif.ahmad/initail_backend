const knex = require('../../../db/knex');
const respondToOperator = require('./respond-to-operator');
const broadcastIstMt = require('./ist-mt-broadcast');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');


const processSMSRequest = async (payload) => {

    try {

        let { requestRawBody, responseRawBody } = payload;
        delete payload.requestRawBody;
        delete payload.responseRawBody;

        console.log('[helpers][operator][dtac][sms]:  PAyload:', JSON.stringify(payload));

        // Try to Register...
        let queryData = {
            query_name: "dtac_register_service_sms",
            ud: payload.commandWord,
            ussd_code: payload.ussdCode,
            product_id: payload.productId,
            short_code: payload.shortCode,
            "channel_rid": 5,
            "partner_rid": 3,
            "operator_rid": 3,
            "dtac_txid": payload.txid,
            "original_message_json": payload,
            "source_address_number": payload.msisdn,
            "destination_address_number": payload.msisdn
        };

        console.log(`[helpers][operator][dtac][sms]: Prepared Query Data (SMS):`, JSON.stringify(queryData));


        let data = await execDbProcedure(queryData);
        // console.log('[helpers][operator][dtac][sms]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][dtac][sms]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][dtac][sms]: Return Value:', JSON.stringify(data));

        const url = data.request_mt_notification.request_register_ip;           // Coming From Response Endpoint Setting


        console.log('[helpers][operator][dtac][sms]: Operator Endpoint (Ip):', url);
        console.log('[helpers][operator][dtac][sms]: Response Type From DB Call:', data.response_type);

        // TODO:  More Logic for respond.......
        if (data.response_type == 'response_on_register') {

            await broadcastIstMt(data.first_mt_info);

        } else if (data.response_type == 'duplicate_registration') {

            // DO NOTHING

        } else if (data.response_type == 'response_on_cancel' || 'duplicate_cancellation') {

            // DO NOTHING

        } else {
            console.log('[helpers][operator][dtac][sms]: Unknow Response Type From DB Call:', data.response_type);
        }


        // Save Incoming Request To Log...
        queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: data.registration_transaction_id,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "I",
            request_json: {
                ...payload,
                requestRawBody: requestRawBody
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                ...data,
                responseRawBody: responseRawBody
            },
            response_timestamp: new Date().toISOString(),
            message_info: ""   // notify_user or notify_operator In case of outgoing
        };

        console.log(`[helpers][operator][dtac][sms]: Saving Incoming Request Log:`, queryData);
        await execDbProcedure(queryData);

        return;

    } catch (err) {
        console.error('[helpers][operator][dtac][sms]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processSMSRequest;
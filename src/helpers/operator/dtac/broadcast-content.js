const axios = require('../../../helpers/axios');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const utilityHelper = require('../../../helpers/utility');
const sendToSQS = require('../../../helpers/queue');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const broadcastContent = async (message) => {

    console.log(`[helpers][dtac][broadcast-content]: Message for Broadcast:`, JSON.stringify(message));

    let contentBroadcasted = false;
    let { member, broadcastContent, operator } = JSON.parse(JSON.stringify(message));

    let url = operator.ip_address;

    if(process.env.APP_ENV == 'local') {
        url = 'http://52.77.249.254'
    }

    try {

        let res = await axios.post(
            url,
            broadcastContent,
            {
                headers: {
                    'Content-Type': 'application/xml',
                }
            }
        );

        console.log(`[helpers][dtac][broadcast-content]: Broadcasted Successfully for ${member.msisdn}, Evaluating Response....`, res.data);

        // Now call db procedure calls to log...
        let queryData = {
            query_name: "round_member_list_update_process",
            record_id: member.record_id,
            updates: [
                { field_name: "on_queue_flag", field_value: 2 },
                { field_name: "success_sent_flag", field_value: 1 },
            ]
        };
        await execDbProcedure(queryData);

        contentBroadcasted = true;

        // Now call db apis to add log...
        // Parse XML Response and convert to json
        let simplifiedJson;
        try {
            let parsedJson = await utilityHelper.parseXMLFromString(res.data);
            simplifiedJson = await utilityHelper.processXMLPayload(parsedJson);
            simplifiedJson.rawBody = res.data;
            console.log('[helpers][dtac][broadcast-content]: Simplified JSON Response from Server:', JSON.stringify(simplifiedJson));
        } catch (err) {
            console.error('[helpers][dtac][broadcast-content]: Unable to Get JSON from Server Response Error:', simplifiedJson);
        }

        queryData = {
            query_name: "broadcast_content_rounds_service_member_log_save",
            record_id: null,
            broadcast_content_rounds_service_member_rid: member.record_id,
            member_rid: member.member_rid,
            server_log_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
            incoming_outgoing_flag: "O",
            request_message: message,
            response_message: simplifiedJson,
            "internal_message_id": null,
            "external_message_id": simplifiedJson['cpa-response'].txid,
            "message_type": 'BRODCAST_COMPLETE',
            "message_status": simplifiedJson['cpa-response'].status,
            "message_code": simplifiedJson['cpa-response'].status,
            "message_description": simplifiedJson['cpa-response']['status-description'],
            "operator_rid": operator.operator_rid
        };
        await execDbProcedure(queryData);

    } catch (err) {

        if (err.response) {
            // client received an error response (5xx, 4xx)
            console.error('[helpers][dtac][broadcast-content]: Failed to broadcast to Operator: Recieved Error Response:', err);
        } else if (err.request) {
            // client never received a response, or request never left
            console.error('[helpers][dtac][broadcast-content]: Failed to broadcast to Operator: No Response:', err);
        } else {
            // anything else
            console.error('[helpers][dtac][broadcast-content]: Some other Error in broadcasting to Operator:', err);
        }

        // TODO: Update the message Resend Logic

        let queryData = {
            query_name: "round_member_list_update_process",
            record_id: member.record_id,
            updates: [
                { field_name: "resent_flag", field_value: 1 }
            ]
        };
        let  resendTryData =  await execDbProcedure(queryData);
        console.log(`[helpers][dtac][broadcast-content]: Resent Flag updated for member: ${member.msisdn}, Resent Flag Retry Data:`, JSON.stringify(resendTryData));

    }

    return contentBroadcasted;

};

module.exports = broadcastContent;
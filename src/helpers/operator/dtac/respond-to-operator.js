const axios = require('../../../helpers/axios');
const utilityHelper = require('../../../helpers/utility');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const respondToOperator = async (data, url) => {

    let timestamp = moment().format('YYYYMMDDHHmmssSSS');

    const ORG_TYPE = '108';
    const TRX_ID_PREFIX = '0242';

    let operationKeyword = 'register';
    switch (data.response_type) {
        case 'response_on_register':
        case 'duplicate_registration':    
            operationKeyword = 'register';
            break;
        case 'response_on_cancel':
        case 'duplicate_cancellation':
            operationKeyword = 'unregister';
            break;
        default:
            operationKeyword = 'register';            
    }

    const xml = `<?xml version="1.0" encoding="UTF-8" ?>
    <cpa-${operationKeyword}>
        <txid>${TRX_ID_PREFIX}${timestamp}</txid>
        <org-type>${ORG_TYPE}</org-type>
        <authentication>
            <user>${data.response_info.other_settings.request_register_username}</user>
            <password>${data.response_info.other_settings.request_register_password}</password>
        </authentication>
        <originator>
            <sender>Gmember</sender>
        </originator>
        <destination>
            <productid>${data.response_info.request_register_product_id}</productid>
            <msisdn>${data.member_info.msisdn}</msisdn>
        </destination>
    </cpa-${operationKeyword}>`;

    console.log('[helpers][operator][dtac][respond-to-operator]: URL:', url);
    console.log('[helpers][operator][dtac][respond-to-operator]: Sending To Operator:', xml);


    try {

        let axiosResponse = await axios.post(
            url,
            xml,
            {
                headers: {
                    'Content-Type': 'application/xml',
                }
            }
        );
    
        console.log('[helpers][operator][dtac][respond-to-operator]: Send Operation Status to Operator Successfully:', axiosResponse.data);
        let parsedJson = await utilityHelper.parseXMLFromString(axiosResponse.data);
        let simplifiedJson = await utilityHelper.processXMLPayload(parsedJson);
    
    
        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xml
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                simplifiedJson: simplifiedJson,
                responseRawBody: axiosResponse.data
            },
            response_timestamp: new Date().toISOString(),
            operator_status_code: simplifiedJson["cpa-subscription-status-reply"] ? simplifiedJson["cpa-subscription-status-reply"]["subscription-status"]["status-code"] : simplifiedJson["cpa-subscription-status-reply"],
            operator_status_description: simplifiedJson["cpa-subscription-status-reply"] ? simplifiedJson["cpa-subscription-status-reply"]["subscription-status"]["status-description"] : simplifiedJson["cpa-subscription-status-reply"],
            message_info: "notify_operator"   // notify_user or notify_operator
        };
    
        console.log(`[helpers][operator][dtac][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

    } catch (err) {
        if (err.response) {
            // client received an error response (5xx, 4xx)
            console.error('[helpers][operator][dtac][respond-to-operator]: Failed to Send Message to Operator: Recieved Error Response:', err.message);
        } else if (err.request) {
            // client never received a response, or request never left
            console.error('[helpers][operator][dtac][respond-to-operator]: Failed to Send Message to Operator: No Response:', err.message);
        } else {
            // anything else
            console.error('[helpers][operator][dtac][respond-to-operator]: Some other Error in Sending Message to Operator:', err);
        }


        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xml
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                simplifiedJson: JSON.stringify(err.message),
                responseRawBody: err
            },
            response_timestamp: new Date().toISOString(),
            operator_status_code: null,
            operator_status_description: 'Network Error',
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][dtac][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

        throw new Error(`Failed to notify Operator about operation status: ${err.message}`);
    }

};

module.exports = respondToOperator;
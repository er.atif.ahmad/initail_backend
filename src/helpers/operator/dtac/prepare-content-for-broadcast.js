const axios = require('axios').default;
const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);

const prepareXMLContentPayloadForSMS = async ({ username, password, broadcast_sender_name, broadcast_service_id }, unicodedResponse) => {
    
    let cpid = '0242';

    return `<?xml version="1.0" encoding="UTF-8" ?>
    <cpa-request>
        <txid>${cpid}TS_YYYYMMDDHHmmssSSS_TS</txid>
        <authentication>
            <user>${username}</user>
            <password>${password}</password>
        </authentication>
        <originator>
            <sender>${broadcast_sender_name}</sender>
        </originator>
        <destination>
            <serviceid>${broadcast_service_id}</serviceid>
            <msisdn>66_MSISDN_99</msisdn>
        </destination>
        <message>
            <header>
                <timestamp>TS_YYYYMMDDHHmmssSSS_TS</timestamp>
            </header>
            <sms>
                <msg>${unicodedResponse}</msg>
                <msgtype>T</msgtype>
                <encoding>8</encoding>
            </sms>
        </message>
    </cpa-request>`;
};



const prepareContentForBroadcast = async (roundData, operatorData) => {

    let contentTypeCode = roundData.content_settings && roundData.content_settings.code ? roundData.content_settings.code : null;
    let smsText;
    let xml;
    let unicodedResponse;


    console.log('Content Type for given content_settings for round id:', contentTypeCode, roundData.bc_round_rid);

    switch (contentTypeCode) {
        case 'txt70':
        case 'txt140':
            smsText = roundData.content_value_json.text_content;
            break;
        case 'txt140_rtf_lnk':
        case 'pick_a_card_4':    
        case 'pick_a_card_6':    
            smsText = roundData.content_value_json.sms_message + ' ' + roundData.content_value_json.url;
            break;
        default:
            console.warn('No Content Type Matched for given content settings for round id:', roundData.bc_round_rid);
    }

    console.log('[helpers][operator][dtac][prepare-content-for-broadcast]: SMS To Be Broadcasted:', smsText);
    xml = await prepareXMLContentPayloadForSMS(operatorData, smsText);
    console.log('[helpers][operator][dtac][prepare-content-for-broadcast]: Content:', xml);
    return xml;

};

module.exports = prepareContentForBroadcast;
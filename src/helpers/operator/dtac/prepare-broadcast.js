const Parallel = require('async-parallel');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const sendToSQS = require('../../../helpers/queue');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const prepareBroadcast = async (round, operator) => {

    try {

        console.log('[helpers][dtac][prepareBroadcast]: Round Data:', JSON.stringify(round));

        console.log('[helpers][dtac][prepareBroadcast]:: Going to prepare member_list for operator :', operator);

        const broadcastTime = moment(round.broadcast_datetime);
        let diffInSeconds = moment.duration(broadcastTime.diff(moment()));
        diffInSeconds = diffInSeconds.asSeconds();

        diffInSeconds = parseInt(diffInSeconds);
        console.log('[helpers][dtac][prepareBroadcast]: Diff In Seconds / Broadcast Time: ', diffInSeconds, round.broadcast_datetime);
        if (diffInSeconds < 1) {
            diffInSeconds = 0;
        }

        let orignalBroadcastContent = operator.broadcastContent;
        delete operator.broadcastContent;


        let queryData = {
            query_name: "round_member_list_prepare",
            service_round_rid: round.service_round_rid,
            operator_rid: operator.operator_rid
        };

        let memberPrepareResult = await execDbProcedure(queryData);
        memberPrepareResult = memberPrepareResult.return_value[0] ? memberPrepareResult.return_value[0] : memberPrepareResult.return_value;
        if (!memberPrepareResult) {
            console.error('[helpers][dtac][prepareBroadcast]: Return Value Error:', memberPrepareResult);
            throw new Error(`Unknown Return Value: ${memberPrepareResult}`);
        }
        let totalRecordsGenerated = memberPrepareResult.total_records;
        console.warn('[helpers][dtac][prepareBroadcast]: Total Records Generated on (round_member_list_prepare):', totalRecordsGenerated);
        // IF no records then no need to continue...
        if (!totalRecordsGenerated || totalRecordsGenerated < 1) {
            console.warn('[helpers][dtac][prepareBroadcast]: No Member subscribers for :', memberPrepareResult, round, operator);
            return;
        }

        const recordsPerIteration = 100;
        const memberFetchQueries = [];

        for (let i = 0; i < Math.ceil(totalRecordsGenerated / recordsPerIteration); i++) {
            queryData = {
                query_name: "round_member_list_get",
                service_round_rid: round.service_round_rid,
                operator_rid: operator.operator_rid,
                from_row_num: (recordsPerIteration * i) + 1,
                to_row_num: (recordsPerIteration * i) + recordsPerIteration
            };
            memberFetchQueries.push(queryData);
        }


        await Parallel.each(memberFetchQueries, async (q) => {
            let members = await execDbProcedure(q);
            members = members.return_value;
            console.log('[helpers][dtac][prepareBroadcast]: Members for operator/round :', operator.operator_rid, round.service_round_rid, q);

            console.log('[helpers][dtac][prepareBroadcast]: Pushing Messages for each members: ', JSON.stringify(members));
            await Parallel.each(members, async (member) => {

                console.log('[helpers][dtac][prepareBroadcast]: Replacing "66_MSISDN_99" with: ', member.msisdn);
                let broadcastContent = `${orignalBroadcastContent}`;
                broadcastContent = broadcastContent.replace("66_MSISDN_99", member.msisdn);
    
                let timestamp = moment().format('YYYYMMDDHHmmssSSS');

                console.log('[helpers][dtac][prepareBroadcast]: Replacing "TS_YYYYMMDDHHmmssSSS_TS" with: ', timestamp);
                broadcastContent = broadcastContent.replace("TS_YYYYMMDDHHmmssSSS_TS", timestamp);
                broadcastContent = broadcastContent.replace("TS_YYYYMMDDHHmmssSSS_TS", timestamp);

                let sqsPayload = {
                    member,
                    broadcastContent,
                    operator,
                    operatorId: operator.operator_rid
                };

                // Add SQS Attributes and push ...
                // console.log('[jobs][processRoundBroadcastPreparedPayload]: After prepartion: Data to be pushed on SQS :', round.broadcast_datetime, JSON.stringify(sqsPayload));

                let sqsMessageAttributes = {
                    "createdAt": {
                        DataType: "String",
                        StringValue: '' + moment().valueOf()
                    },
                    "messageType": {
                        DataType: "String",
                        StringValue: 'BROADCAST_MESSAGE_CONTENT'
                    }
                };

                let sqsPublishResponse = await sendToSQS(sqsPayload, sqsMessageAttributes, diffInSeconds);
                console.log('[jobs][processRoundBroadcastPreparedPayload]: SQS Publish Response /Delivery delay:', sqsPublishResponse, diffInSeconds);

                // Now update that this message is on queue
                let queryData = {
                    query_name: "round_member_list_update_process",
                    record_id: member.record_id,
                    updates: [
                        { field_name: "on_queue_flag", field_value: 1 }
                    ]
                };
                await execDbProcedure(queryData);

            });

        });


        // Call DB Proc to inform that these rounds are on queue for broadcasting...
        queryData = {
            query_name: "prepare_daily_broadcast_update_process",
            updates: [
                { service_round_rid: round.service_round_rid, "process_status": 2 },
            ]
        };
        await execDbProcedure(queryData);
        console.log('[helpers][dtac][prepareBroadcast]: Updated Process Status to 2 in DB:');


    } catch (err) {
        console.error('[helpers][dtac][prepareBroadcast]: Some Error in putting up to broadcast queue:', err);

        for (var item of err.list) {
            console.log('[helpers][dtac][prepareBroadcast]: Each Error1: ', item.message);

            for (var it of item.list) {
                console.log('[helpers][dtac][prepareBroadcast]: Each Error2: ', it.message);

            }
        }
    }

};

module.exports = prepareBroadcast;
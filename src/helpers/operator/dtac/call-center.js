const knex = require('../../../db/knex');
const respondToOperator = require('./respond-to-operator');
const execDbProcedure = require('../../db/exec-db-procedure');
const broadcastIstMt = require('./ist-mt-broadcast');



const processCallCenterRequest = async (payload) => {

    try {

        console.log('[helpers][operator][dtac][call-center]:  Payload:', JSON.stringify(payload));

        // Try to Register...
        let queryData = {
            query_name: "dtac_register_service_callcenter",
            ud: payload.message.cc.ud._,
            operator_rid: payload.operatorId,
            channel_rid: payload.channelId,
            partner_rid: payload.operatorId,            // Same as operator rid
            "dtac_txid": null,
            "original_message_json": payload,
            "source_address_number": payload.message.cc.source.address._,
            product_id: payload.productId,
            service_id: payload.message.cc['service-id']
        };

        console.log(`[helpers][operator][dtac][call-center]: Prepared Query Data (SMS):`, JSON.stringify(queryData));


        let data = await execDbProcedure(queryData);
        // console.log('[helpers][operator][dtac][call-center]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][dtac][call-center]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][dtac][call-center]: Return Value:', JSON.stringify(data));

        const url = data.request_mt_notification.request_register_ip;                         // Coming From Response Endpoint Setting
        const operatorPushUrl = data.response_info.other_settings.request_register_ip;        // Coming From Register/Cancel Endpoint Setting Conditionally


        console.log('[helpers][operator][dtac][call-center]: Operator Endpoint (Ip):', url);
        console.log('[helpers][operator][dtac][call-center]: Response Type From DB Call:', data.response_type);

        if (data.response_type == 'response_on_register') {

            await respondToOperator(data, operatorPushUrl);

            await broadcastIstMt(data.first_mt_info);

        } else if (data.response_type == 'duplicate_registration') {

            await respondToOperator(data, operatorPushUrl);

        } else if (data.response_type == 'wrong_format') {


        } else if (data.response_type == 'response_on_cancel' || 'duplicate_cancellation') {

            await respondToOperator(data, operatorPushUrl);

        } else {
            console.log('[helpers][operator][dtac][call-center]: Unknow Response Type From DB Call:', data.response_type);
        }

        return data;

    } catch (err) {
        console.error('[helpers][operator][dtac][call-center]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processCallCenterRequest;
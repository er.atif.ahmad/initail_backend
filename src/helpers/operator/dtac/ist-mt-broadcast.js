const Parallel = require('async-parallel');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const sendToSQS = require('../../../helpers/queue');
const prepareContentHelperDtac = require('../../../helpers/operator/dtac/prepare-content-for-broadcast');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const broadcastIstMt = async (payload) => {

    try {

        console.log('[helpers][dtac][broadcastIstMt]: Ist Mt Payload:', JSON.stringify(payload));

        if(!payload) {
            console.warn('[helpers][dtac][broadcastIstMt]: No Config Passed For 1st Mt Broadcast, hence skipping');
            return;
        }

        let round = payload.prepare_daily_broadcast_info;
        let member = payload.round_member_list_get_info;

        if (!round || !round.operator_list || round.operator_list.length < 1) {
            console.warn('[helpers][dtac][broadcastIstMt]: No operators configured for given round...');
            return;
        }

        await Parallel.each(round.operator_list, async (operator) => {
            console.log('[helpers][dtac][broadcastIstMt]: Going to prepare content for operator :', operator.code);
            switch (operator.operator_rid) {
                case 3:   // DTAC
                    operator.broadcastContent = await prepareContentHelperDtac(round, operator);
                    break;
                default:
                    console.warn('[helpers][dtac][broadcastIstMt]: No matching operator found...');
            }
        });

        console.log(`[helpers][dtac][broadcastIstMt]: Message payload (Broadcast Round/Package):`, JSON.stringify(round));

        let timestamp = moment().format('YYYYMMDDHHmmssSSS');

        await Parallel.each(round.operator_list, async (operator) => {

            let orignalBroadcastContent = operator.broadcastContent;
            delete operator.broadcastContent;

            console.log('[helpers][dtac][broadcastIstMt]: Replacing "66_MSISDN_99" with: ', member.msisdn);
            let broadcastContent = `${orignalBroadcastContent}`;
            broadcastContent = broadcastContent.replace("66_MSISDN_99", member.msisdn);


            console.log('[helpers][dtac][broadcastIstMt]: Replacing "TS_YYYYMMDDHHmmssSSS_TS" with: ', timestamp);
            broadcastContent = broadcastContent.replace("TS_YYYYMMDDHHmmssSSS_TS", timestamp);
            broadcastContent = broadcastContent.replace("TS_YYYYMMDDHHmmssSSS_TS", timestamp);

            let sqsPayload = {
                member,
                broadcastContent,
                operator
            };

            let sqsMessageAttributes = {
                "createdAt": {
                    DataType: "String",
                    StringValue: '' + moment().valueOf()
                },
                "messageType": {
                    DataType: "String",
                    StringValue: 'BROADCAST_MESSAGE_CONTENT'
                }
            };

            let sqsPublishResponse = await sendToSQS(sqsPayload, sqsMessageAttributes, 0);
            console.log('[helpers][dtac][broadcastIstMt]: SQS Publish Response:', sqsPublishResponse);

            // Now update that this message is on queue
            let queryData = {
                query_name: "round_member_list_update_process",
                record_id: member.record_id,
                updates: [
                    { field_name: "on_queue_flag", field_value: 1 }
                ]
            };
            await execDbProcedure(queryData);

        });


    } catch (err) {
        console.error('[helpers][dtac][broadcastIstMt]: Some Error in broadcasting Ist Mt:', err);

        for (var item of err.list) {
            console.log('[helpers][dtac][broadcastIstMt]: Each Error1: ', item.message);

            for (var it of item.list) {
                console.log('[helpers][dtac][broadcastIstMt]: Each Error2: ', it.message);

            }
        }
    }

};

module.exports = broadcastIstMt;
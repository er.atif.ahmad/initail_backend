const knex = require('../../../db/knex');
const notifyCustomer = require('./notify-customer');
const respondToOperator = require('./respond-to-operator');
const broadcastIstMt = require('./ist-mt-broadcast');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');


const processUSSDRequest = async (payload) => {

    try {

        let  {requestRawBody, responseRawBody} = payload;
        delete payload.requestRawBody;
        delete payload.responseRawBody;
    

        // Try to Register...
        let queryData = {
            query_name: "true_register_service_ussd",
            operator_rid: payload.operatorId,
            channel_rid: payload.channelId,
            partner_rid: payload.operatorId,            // Same as operator rid
            destination_address_number: payload.message.sms.destination.address._,
            ud: payload.message.sms.ud._,
            source_address_number: payload.message.sms.source.address._,
            original_message_json: payload,
            ussd_code: payload.ussdCode,
            short_code: payload.shortCode
        };

        console.log(`[helpers][operator][true][ussd]: Prepared Query Data (USSD):`, JSON.stringify(queryData));

        let data = await execDbProcedure(queryData);
        // console.log('[helpers][operator][true][ussd]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][true][ussd]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][true][ussd]: Return Value:', JSON.stringify(data));

        const operatorPushUrl = data.response_info.other_settings.request_register_ip;
        const url = data.request_mt_notification.request_register_ip;

        console.log('[helpers][operator][true][ussd]: Operator Endpoint For Operation Notification', operatorPushUrl);
        console.log('[helpers][operator][true][ussd]: Operator Endpoint (Ip):', url);
        console.log('[helpers][operator][true][ussd]: Response Type From DB Call:', data.response_type);

        if(url) {
            if (data.response_type == 'response_on_register') {
            
                await respondToOperator(data, operatorPushUrl);
    
                await notifyCustomer(data, url);
    
                await broadcastIstMt(data.first_mt_info);
    
            } else if (data.response_type == 'duplicate_registration') {
    
                await respondToOperator(data, operatorPushUrl);
    
                await notifyCustomer(data, url);
    
            } else if (data.response_type == 'wrong_format') {
    
                await notifyCustomer(data, url);
    
            } else {
                console.log('[helpers][operator][true][ussd]: Unknow Response Type From DB Call:', data.response_type);
            }
            
        } else {
            console.warn(`[helpers][operator][true][ussd]: Operator Endpoint is null, Hence Aborting, Please check setup`);
        }

        // Save Incoming Request To Log...
        queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: data.registration_transaction_id,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "I",
            request_json: {
                ...payload,
                requestRawBody: requestRawBody
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                ...data,
                responseRawBody: responseRawBody
            },
            response_timestamp: new Date().toISOString(),
            message_info: ""   // notify_user or notify_operator In case of outgoing
        };

        console.log(`[helpers][operator][true][ussd]:  Saving Incoming Request Log:`, queryData);
        await execDbProcedure(queryData);

        return data;

    } catch (err) {
        console.error('[helpers][operator][true][ussd]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processUSSDRequest;
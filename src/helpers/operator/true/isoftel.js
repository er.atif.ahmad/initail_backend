const knex = require('../../../db/knex');
const notifyCustomer = require('./notify-customer');
const respondToOperator = require('./respond-to-operator');
const execDbProcedure = require('../../db/exec-db-procedure');
const broadcastIstMt = require('./ist-mt-broadcast');
const Parallel = require('async-parallel');



const processIsofTelRequest = async (payload) => {

    try {

        let { requestRawBody, responseRawBody } = payload;
        delete payload.requestRawBody;
        delete payload.responseRawBody;

        console.log(`[helpers][operator][true][isoftel]: Payload:`, payload);

        let msisdnArr = [];

        if (payload.request.size > 1) {
            // Handle logic to get all msisdn values and push to arr
            msisdnArr = [...Object.values(payload.request.msisdn)]
        } else {
            msisdnArr.push(payload.request.msisdn);
        }

        console.log(`[helpers][operator][true][isoftel]: Msisdn Array:`, msisdnArr);


        await Parallel.each(msisdnArr, async (msisdn) => {

            let queryData = {
                query_name: "telesales_isoftel_register_service",
                operator_rid: payload.operatorId,
                channel_rid: payload.channelId,
                partner_rid: payload.partnerId,
                destination_address_number: payload.request.service,
                ud: payload.udText,
                source_address_number: msisdn,
                original_message_json: payload,
                ussd_code: null,
                short_code: payload.request.service,
                service_id: null
            };

            console.log(`[helpers][operator][true][isoftel]: Prepared Query Data:`, queryData);


            let data = await execDbProcedure(queryData);
            console.log('[helpers][operator][true][isoftel]: Data From Query:', JSON.stringify(data));

            data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

            if (!data) {
                console.error(`[helpers][operator][true][isoftel]: Query Result From DB gives Return Value Null Hence cannot process it further`);
                throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
            }

            console.log('[helpers][operator][true][isoftel]: Return Value:', JSON.stringify(data));

            const operatorPushUrl = data.response_info.other_settings.request_register_ip;
            const url = data.request_mt_notification.request_register_ip;

            console.log('[helpers][operator][true][isoftel]: Operator Endpoint For Operation Notify:', operatorPushUrl);
            console.log('[helpers][operator][true][isoftel]: Operator Endpoint (Ip):', url);
            console.log('[helpers][operator][true][isoftel]: Response Type From DB Call:', data.response_type);

            if (data.response_type == 'response_on_register') {

                await respondToOperator(data, operatorPushUrl);

                await notifyCustomer(data, url);

                await broadcastIstMt(data.first_mt_info);

            } else if (data.response_type == 'duplicate_registration') {

                await respondToOperator(data, operatorPushUrl);

                await notifyCustomer(data, url);

            } else if (data.response_type == 'response_on_cancel' || 'duplicate_cancellation') {

                await respondToOperator(data, operatorPushUrl);

            } else {
                console.log('[helpers][operator][true][isoftel]: Unknow Response Type From DB Call:', data.response_type);
            }

            return data;

        });

        return ;




    } catch (err) {
        console.error('[helpers][operator][true][isoftel]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processIsofTelRequest;
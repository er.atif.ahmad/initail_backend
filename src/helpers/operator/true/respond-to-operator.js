const axios = require('../../../helpers/axios');
const utilityHelper = require('../../../helpers/utility');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');


const respondToOperator = async (data, url) => {

    if (process.env.APP_ENV == 'local') {
        url = 'http://52.77.249.254'
    }

    const xml = `<?xml version="1.0" encoding="UTF-8"?>
    <request type="mo" id="${data.registration_transaction_id}">
        <body>
            <number>${data.member_info.msisdn}</number>
            <service-id>${data.response_info.request_register_service_id}</service-id>
            <ud>${data.response_info.other_settings.request_ud}</ud>
            <authorization>${data.response_info.other_settings.request_register_authorization}</authorization>
            <channel>${data.response_info.other_settings.request_register_channel}</channel>
        </body>
    </request>`;


    console.log('[helpers][operator][true][respond-to-operator]: Sending XML Body Using Url:', url);
    console.log('[helpers][operator][true][respond-to-operator]: Sending XML Basic Authorization:', data.response_info.other_settings.request_register_authorization);
    console.log('[helpers][operator][true][respond-to-operator]: Sending XML Body:', xml);


    // Get the Operator Ip Address for sending these XML Messages...
    // For Staging ==> 52.77.249.254

    try {

        let axiosResponse = await axios.post(
            url,
            xml,
            {
                headers: {
                    'Content-Type': 'text/xml',
                    'Charset': 'TIS-620',
                    'Authorization': `Basic ${data.response_info.other_settings.request_register_authorization}`
                }
            }
        );

        console.log('[helpers][operator][true][respond-to-operator]: Send Operation Status to Operator Successfully:', axiosResponse.data);
        let parsedJson = await utilityHelper.parseXMLFromString(axiosResponse.data);
        let simplifiedJson = await utilityHelper.processXMLPayload(parsedJson);


        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xml
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                simplifiedJson: simplifiedJson,
                responseRawBody: axiosResponse.data
            },
            response_timestamp: new Date().toISOString(),
            operator_status_code: simplifiedJson.response ? simplifiedJson.response.body.status : simplifiedJson.response,
            operator_status_description: simplifiedJson.response ? simplifiedJson.response.body.description : simplifiedJson.response,
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][true][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

    } catch (err) {

        if (err.response) {
            // client received an error response (5xx, 4xx)
            console.error('[helpers][operator][true][respond-to-operator]: Failed to Send Message to Operator: Recieved Error Response:', err.message);
        } else if (err.request) {
            // client never received a response, or request never left
            console.error('[helpers][operator][true][respond-to-operator]: Failed to Send Message to Operator: No Response:', err.message);
        } else {
            // anything else
            console.error('[helpers][operator][true][respond-to-operator]: Some other Error in Sending Message to Operator:', err);
        }


        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xml
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                simplifiedJson: JSON.stringify(err.message),
                responseRawBody: err
            },
            response_timestamp: new Date().toISOString(),
            operator_status_code: null,
            operator_status_description: 'Network Error',
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][true][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

        throw new Error(`Failed to notify Operator about operation status: ${err.message}`);

    }

};

module.exports = respondToOperator;
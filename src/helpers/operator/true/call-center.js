const knex = require('../../../db/knex');
const notifyCustomer = require('./notify-customer');
const respondToOperator = require('./respond-to-operator');
const execDbProcedure = require('../../db/exec-db-procedure');
const broadcastIstMt = require('./ist-mt-broadcast');



const processCallCenterRequest = async (payload) => {

    try {

        let queryData = {
            query_name: "true_register_service_callcenter",
            operator_rid: payload.operatorId,
            channel_rid: payload.channelId,
            partner_rid: payload.operatorId,            // Same as operator rid
            destination_address_number: payload.message.cc.destination.address._,
            ud: payload.message.cc.ud._,
            source_address_number: payload.message.cc.source.address._,
            original_message_json: payload,
            ussd_code: payload.ussdCode,
            short_code: payload.shortCode,
            service_id: payload.message.cc['service-id']
        };

        console.log(`[helpers][operator][true][call-center]: Prepared Query Data (CallCenter):`, queryData);


        let data = await execDbProcedure(queryData);
        console.log('[helpers][operator][true][call-center]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][true][call-center]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][true][call-center]: Return Value:', JSON.stringify(data));

        const operatorPushUrl = data.response_info.other_settings.request_register_ip;
        const url = data.request_mt_notification.request_register_ip;

        console.log('[helpers][operator][true][call-center]: Operator Endpoint For Operation Notify:', operatorPushUrl);
        console.log('[helpers][operator][true][call-center]: Operator Endpoint (Ip):', url);
        console.log('[helpers][operator][true][call-center]: Response Type From DB Call:', data.response_type);

        if (data.response_type == 'response_on_register') {

            await respondToOperator(data, operatorPushUrl);

            await notifyCustomer(data, url);

            await broadcastIstMt(data.first_mt_info);

        } else if (data.response_type == 'duplicate_registration') {

            await respondToOperator(data, operatorPushUrl);

            await notifyCustomer(data, url);

        } else if (data.response_type == 'wrong_format') {

            await notifyCustomer(data, url);

        } else if (data.response_type == 'response_on_cancel' || 'duplicate_cancellation') {

            await respondToOperator(data, operatorPushUrl);

            // await notifyCustomer(data, url);

        } else {
            console.log('[helpers][operator][true][call-center]: Unknow Response Type From DB Call:', data.response_type);
        }

        return data;

    } catch (err) {
        console.error('[helpers][operator][true][call-center]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processCallCenterRequest;
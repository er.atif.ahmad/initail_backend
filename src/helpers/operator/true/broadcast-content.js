const axios = require('../../../helpers/axios');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const utilityHelper = require('../../../helpers/utility');
const sendToSQS = require('../../../helpers/queue');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const respondToOperator = async (message) => {

    console.log(`[helpers][trueh][broadcast-content]: Message for Broadcast:`, JSON.stringify(message));

    let contentBroadcasted = false;
    let { member, broadcastContent, operator } = JSON.parse(JSON.stringify(message));

    let url = operator.ip_address;

    if(process.env.APP_ENV == 'local') {
        url = 'http://52.77.249.254'
    }

    try {

        let res = await axios.post(
            url,
            broadcastContent,
            {
                headers: {
                    'Content-Type': 'text/xml',
                    'Charset': 'TIS-620',
                    'Authorization': `Basic ${operator.authorization_code}`
                }
            }
        );

        console.log(`[helpers][trueh][broadcast-content]: Broadcasted Successfully for ${member.msisdn}, Evaluating Response....`);

        // Now call db procedure calls to log...
        let queryData = {
            query_name: "round_member_list_update_process",
            record_id: member.record_id,
            updates: [
                { field_name: "on_queue_flag", field_value: 2 },
                { field_name: "success_sent_flag", field_value: 1 },
            ]
        };
        await execDbProcedure(queryData);

        contentBroadcasted = true;

        // Now call db apis to add log...
        // Parse XML Response and convert to json
        let simplifiedJson;
        try {
            let parsedJson = await utilityHelper.parseXMLFromString(res.data);
            simplifiedJson = await utilityHelper.processXMLPayload(parsedJson);
            simplifiedJson.rawBody = res.data;
            console.log('[helpers][trueh][broadcast-content]: Simplified JSON Response from Server:', JSON.stringify(simplifiedJson));
        } catch (err) {
            console.error('[helpers][trueh][broadcast-content]: Unable to Get JSON from Server Response Error:', simplifiedJson);
        }

        queryData = {
            query_name: "broadcast_content_rounds_service_member_log_save",
            record_id: null,
            broadcast_content_rounds_service_member_rid: member.record_id,
            member_rid: member.member_rid,
            server_log_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
            incoming_outgoing_flag: "O",
            request_message: message,
            response_message: simplifiedJson,
            "internal_message_id": null,
            "external_message_id": simplifiedJson.message.id,
            "message_type": simplifiedJson.message.rsr.type,
            "message_status": simplifiedJson.message.rsr.rsr_detail.status,
            "message_code": simplifiedJson.message.rsr.rsr_detail.code,
            "message_description": simplifiedJson.message.rsr.rsr_detail.description,
            "operator_rid": operator.operator_rid
        };
        await execDbProcedure(queryData);

    } catch (err) {

        if (err.response) {
            // client received an error response (5xx, 4xx)
            console.error('[helpers][trueh][broadcast-content]: Failed to broadcast to Operator: Recieved Error Response:', err);
        } else if (err.request) {
            // client never received a response, or request never left
            console.error('[helpers][trueh][broadcast-content]: Failed to broadcast to Operator: No Response:', err);
        } else {
            // anything else
            console.error('[helpers][trueh][broadcast-content]: Some other Error in broadcasting to Operator:', err);
        }

        // TODO: Update the message Resend Logic

        let queryData = {
            query_name: "round_member_list_update_process",
            record_id: member.record_id,
            updates: [
                { field_name: "resent_flag", field_value: 1 }
            ]
        };
        let  resendTryData =  await execDbProcedure(queryData);
        console.log(`[helpers][trueh][broadcast-content]: Resent Flag updated for member: ${member.msisdn}, Resent Flag Retry Data:`, JSON.stringify(resendTryData));

        


        // if (!member.resent_flag) {
        //     member.resent_flag = 0;
        // }

        // if (member.resent_flag < 3) {  // Add condition to check if retries/resend are allowed...
        //     // Now call db procedure calls to log...
        //     console.log(`[helpers][trueh][broadcast-content]: Failed to notify Operator about operation status for: ${member.msisdn}, Attempt to resend again..`, member);

        //     member.resent_flag = member.resent_flag + 1;
        //     let deliveryDelay = 60  // 1 minute
        //     console.log(`[helpers][trueh][broadcast-content]: Publishing for Resend on Sqs, Resent Count:`, member.resent_flag);

        //     message.member = member;

        //     let sqsMessageAttributes = {
        //         "createdAt": {
        //             DataType: "String",
        //             StringValue: '' + moment().valueOf()
        //         },
        //         "messageType": {
        //             DataType: "String",
        //             StringValue: 'BROADCAST_MESSAGE_CONTENT'
        //         }
        //     };

        //     let sqsPublishResponse = await sendToSQS(message, sqsMessageAttributes, deliveryDelay);
        //     console.log(`[helpers][trueh][broadcast-content]: SQS Publish Response (For Resend): After delivery delay: ${deliveryDelay}:`, sqsPublishResponse);

        //     let queryData = {
        //         query_name: "round_member_list_update_process",
        //         record_id: member.record_id,
        //         updates: [
        //             { field_name: "resent_flag", field_value: member.resent_flag }
        //         ]
        //     };
        //     await execDbProcedure(queryData);
        //     console.log(`[helpers][trueh][broadcast-content]: Resent Flag updated for member:`, member.msisdn);


        // } else {

        //     let queryData = {
        //         query_name: "round_member_list_update_process",
        //         record_id: member.record_id,
        //         updates: [
        //             { field_name: "permanent_fail_flag", field_value: 1 }
        //         ]
        //     };
        //     await execDbProcedure(queryData);
        //     console.log(`[helpers][trueh][broadcast-content]: Set permanent_fail_flag to 1 for Msisdn: ${member.msisdn}`);

        // }

    }

    return contentBroadcasted;

};

module.exports = respondToOperator;
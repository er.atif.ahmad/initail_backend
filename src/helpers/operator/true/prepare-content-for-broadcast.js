const axios = require('axios').default;
const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const toUnicode = (str) => {
    return str.split('').map(function (value, index, array) {
        let temp = value.charCodeAt(0).toString(16).toUpperCase();
        let tempDecimal = parseInt(temp, 16);
        let hexaDecimal = '0' + parseInt(tempDecimal).toString(16);
        // console.log(`Decimal For ${value} is :`, tempDecimal);
        console.log(`Hexa For ${value} is :`, hexaDecimal);
        return '&#x' + hexaDecimal + ';';
    }).join('');
}

const prepareXMLContentPayloadForSMS = async ({ broadcast_service_id, broadcast_sender_name }, unicodedResponse) => {
    return `<?xml version="1.0" encoding="TIS-620"?>
    <message>
        <sms type="mt">
            <service-id>${broadcast_service_id}</service-id>
            <destination>
                <address>
                    <number type="international">66_MSISDN_99</number>
                </address>
            </destination>
            <source>
                <address>
                    <number type="abbreviated">${broadcast_sender_name}</number>
                    <originate type="international">66_MSISDN_99</originate>
                    <sender>${broadcast_sender_name}</sender>
                </address>
            </source>
            <ud type="text" encoding="unicode">${unicodedResponse}</ud>
            <stcs>${moment().format()}</stcs>
            <dro>true</dro>
        </sms>
    </message>`;
};


const prepareContentForBroadcast = async (roundData, operatorData) => {

    let contentTypeCode = roundData.content_settings && roundData.content_settings.code ? roundData.content_settings.code : null;
    let smsText;
    let xml;
    let unicodedResponse;

    console.log('Content Type for given content_settings for round id:', contentTypeCode, roundData.bc_round_rid);

    switch (contentTypeCode) {
        case 'txt70':
        case 'txt140':
            smsText = roundData.content_value_json.text_content;
            break;
        case 'txt140_rtf_lnk':
        case 'pick_a_card_4':    
        case 'pick_a_card_6':
            smsText = roundData.content_value_json.sms_message + ' ' + roundData.content_value_json.url;
            break;
        default:
            console.warn('No Content Type Matched for given content settings for round id:', roundData.bc_round_rid);
    }

    console.log('[helpers][operator][true][prepare-content-for-broadcast]: SMS To Be Broadcasted:', smsText);
    unicodedResponse = toUnicode(smsText);
    xml = await prepareXMLContentPayloadForSMS(operatorData, unicodedResponse);
    console.log('[helpers][operator][true][prepare-content-for-broadcast]: Content:', xml);

    return xml;

};

module.exports = prepareContentForBroadcast;
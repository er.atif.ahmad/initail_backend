const axios = require('../../../helpers/axios');
const utilityHelper = require('../../../helpers/utility');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
// const utf8 = require('utf8');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const toUnicode = (str) => {
    return str.split('').map(function (value, index, array) {
        let temp = value.charCodeAt(0).toString(16).toUpperCase();
        let tempDecimal = parseInt(temp, 16);
        let hexaDecimal = '0' + parseInt(tempDecimal).toString(16);
        // console.log(`Decimal For ${value} is :`, tempDecimal);
        console.log(`Hexa For ${value} is :`, hexaDecimal);
        return '&#x' + hexaDecimal + ';';
    }).join('');
}


const notifyCustomer = async (data, url) => {

    if(process.env.APP_ENV == 'local') {
        url = 'http://52.77.249.254'
    }

    // Now prepare XML again and send back to operator to be sent to customer...
    let unicodedResponse = toUnicode(data.request_mt_notification.request_text);
    const xmlCustomer = `<?xml version="1.0" encoding="TIS-620"?>
    <message>
        <sms type="mt">
            <service-id>${data.request_mt_notification.request_service_id}</service-id>
            <destination>
                <address>
                    <number type="international">${data.member_info.msisdn}</number>
                </address>
            </destination>
            <source>
                <address>
                    <number type="abbreviated">${data.response_info.request_register_short_code}</number>
                    <originate type="international">${data.member_info.msisdn}</originate>
                    <sender>${data.request_mt_notification.request_sender_name}</sender>
                </address>
            </source>
            <ud type="text" encoding="unicode">${unicodedResponse}</ud>
            <stcs>${moment().format()}</stcs>
            <dro>true</dro>
        </sms>
    </message>
    `;

    try {
        
        let axiosResponse = await axios.post(
            url,
            xmlCustomer,
            {
                headers: {
                    'Content-Type': 'text/xml',
                    'Charset': 'TIS-620',
                    'Authorization': `Basic ${data.request_mt_notification.request_authorization_code}`
                }
            }
        );

        console.log('[helpers][operator][true][notify-customer]: Operator Response Message for Customer Notification:', axiosResponse.data);
        let parsedJson = await utilityHelper.parseXMLFromString(axiosResponse.data);
        let simplifiedJson = await utilityHelper.processXMLPayload(parsedJson);

        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xmlCustomer
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                simplifiedJson: simplifiedJson,
                responseRawBody: axiosResponse.data
            },
            response_timestamp: new Date().toISOString(),
            message_info: "notify_user"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][true][notify-customer]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

    } catch (err) {

        if (err.response) {
            // client received an error response (5xx, 4xx)
            console.error('[helpers][operator][true][notify-customer]:: Failed to Send Message to Operator: Recieved Error Response:', err.message);
        } else if (err.request) {
            // client never received a response, or request never left
            console.error('[helpers][operator][true][notify-customer]:: Failed to Send Message to Operator: No Response:', err.message);
        } else {
            // anything else
            console.error('[helpers][operator][true][notify-customer]:: Some other Error in Sending Message to Operator:', err);
        }

        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: xmlCustomer
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                simplifiedJson: JSON.stringify(err.message),
                responseRawBody: err
            },
            response_timestamp: new Date().toISOString(),
            operator_status_code: null,
            operator_status_description: 'Network Error',
            message_info: "notify_user"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][true][notify-customer]: Saving Error Response Data:`, queryData);
        await execDbProcedure(queryData);


        throw new Error(`Failed to Send Operator Message for Customer Notification: ${err.message}`);
    }

};

module.exports = notifyCustomer;
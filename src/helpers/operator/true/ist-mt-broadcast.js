const Parallel = require('async-parallel');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const sendToSQS = require('../../../helpers/queue');
const prepareContentHelperTrueh = require('../../../helpers/operator/true/prepare-content-for-broadcast');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const broadcastIstMt = async (payload) => {

    try {

        console.log('[helpers][broadcastIstMt]: Ist Mt Payload:', JSON.stringify(payload));

        if(!payload) {
            console.warn('[helpers][broadcastIstMt]: No Config Passed For 1st Mt Broadcast, hence skipping');
            return;
        }

        let round = payload.prepare_daily_broadcast_info;
        let member = payload.round_member_list_get_info;

        if (!round || !round.operator_list || round.operator_list.length < 1) {
            console.warn('[helpers][broadcastIstMt]: No operators configured for given round...');
            return;
        }

        await Parallel.each(round.operator_list, async (operator) => {
            console.log('[helpers][broadcastIstMt]: Going to prepare content for operator :', operator.code);
            switch (operator.operator_rid) {
                case 2:   // TrueH
                    operator.broadcastContent = await prepareContentHelperTrueh(round, operator);
                    break;
                default:
                    console.warn('[helpers][broadcastIstMt]: No matching operator found...');
            }
        });

        console.log(`[helpers][broadcastIstMt]: Message payload (Broadcast Round/Package):`, JSON.stringify(round));

        await Parallel.each(round.operator_list, async (operator) => {

            let orignalBroadcastContent = operator.broadcastContent;
            delete operator.broadcastContent;

            console.log('[helpers][broadcastIstMt]: Replacing "66_MSISDN_99" with: ', member.msisdn);
            let broadcastContent = `${orignalBroadcastContent}`;
            broadcastContent = broadcastContent.replace("66_MSISDN_99", member.msisdn);
            broadcastContent = broadcastContent.replace("66_MSISDN_99", member.msisdn);

            let sqsPayload = {
                member,
                broadcastContent,
                operator
            };

            let sqsMessageAttributes = {
                "createdAt": {
                    DataType: "String",
                    StringValue: '' + moment().valueOf()
                },
                "messageType": {
                    DataType: "String",
                    StringValue: 'BROADCAST_MESSAGE_CONTENT'
                }
            };

            let sqsPublishResponse = await sendToSQS(sqsPayload, sqsMessageAttributes, 0);
            console.log('[helpers][broadcastIstMt]: SQS Publish Response:', sqsPublishResponse);

            // Now update that this message is on queue
            let queryData = {
                query_name: "round_member_list_update_process",
                record_id: member.record_id,
                updates: [
                    { field_name: "on_queue_flag", field_value: 1 }
                ]
            };
            await execDbProcedure(queryData);

        });


    } catch (err) {
        console.error('[helpers][broadcastIstMt]: Some Error in broadcasting Ist Mt:', err);

        for (var item of err.list) {
            console.log('[helpers][broadcastIstMt]: Each Error1: ', item.message);

            for (var it of item.list) {
                console.log('[helpers][broadcastIstMt]: Each Error2: ', it.message);

            }
        }
    }

};

module.exports = broadcastIstMt;
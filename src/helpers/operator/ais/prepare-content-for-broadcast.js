const axios = require('axios').default;
const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);
const LanguageDetect = require('languagedetect');
const lngDetector = new LanguageDetect();


const prepareXMLContentPayloadForSMS = async ({ service_info, operator_list }, broadcastContent) => {

    const detectedLanguage = lngDetector.detect(broadcastContent, 5);
    console.log('[helpers][operator][ais][prepare-content-for-broadcast]: Detected Language: ', detectedLanguage);

    let isEnglish = detectedLanguage.findIndex(x => x[0] === 'english');
    console.log('[helpers][operator][ais][prepare-content-for-broadcast]: Is English Language: ', isEnglish > -1);

    if (isEnglish) {
        broadcastContent = `E:${broadcastContent}`;
    } else {
        console.log('[helpers][operator][ais][prepare-content-for-broadcast]: Language is not english. So assuming Thai');
        broadcastContent = `T:${broadcastContent}`;
    }

    return `BEGIN__XX_FORMATTED_TS_XX__
SERVICENAME:${service_info.service_name}
PRICE:${operator_list[0].mt_per_message}
TYPE:${operator_list[0].other_settings_by_operator.ais_broadcast_cpaction}
CCT:${operator_list[0].other_settings_by_operator.ais_broadcast_cct}
SENDER:${operator_list[0].broadcast_sender_name}
URL:http://13.248.177.177/api/v1/operator-service-calls/ais/${operator_list[0].broadcast_service_id}
IP:13.248.177.177
PORT:80
${broadcastContent}
66XX_MOBILE_NUMBERS_XX99
END`;


};



const prepareContentForBroadcast = async (roundData, operatorData) => {

    let formattedTimestamp = moment().add(5, 'minutes').format('YYYYMMDDHHmmss');

    let contentTypeCode = roundData.content_settings && roundData.content_settings.code ? roundData.content_settings.code : null;
    let smsText;
    let fileContent = '';

    console.log('[helpers][operator][ais][prepare-content-for-broadcast]: Content Type for given content_settings for round id:', contentTypeCode, roundData.bc_round_rid);

    switch (contentTypeCode) {
        case 'txt70':
        case 'txt140':
            smsText = roundData.content_value_json.text_content;
            break;
        case 'txt140_rtf_lnk':
        case 'pick_a_card_4':
        case 'pick_a_card_6':
            smsText = roundData.content_value_json.sms_message + ' ' + roundData.content_value_json.url;
            break;
        default:
            console.warn('[helpers][operator][ais][prepare-content-for-broadcast]: No Content Type Matched for given content settings for round id:', roundData.bc_round_rid);
    }

    console.log('[helpers][operator][ais][prepare-content-for-broadcast]: SMS To Be Broadcasted:', smsText);
    fileContent = await prepareXMLContentPayloadForSMS(roundData, smsText);
    console.log('[helpers][operator][ais][prepare-content-for-broadcast]: Content (In File):', fileContent);
    return fileContent;

};

module.exports = prepareContentForBroadcast;
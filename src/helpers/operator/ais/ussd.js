const knex = require('../../../db/knex');
const respondToOperator = require('./respond-to-operator');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');


const processUSSDRequest = async (payload) => {

    try {

        let { requestRawBody, responseRawBody } = payload;
        delete payload.requestRawBody;
        delete payload.responseRawBody;

        console.log('[helpers][operator][ais][ussd]:  PAyload:', JSON.stringify(payload));

        let udText = '';
        switch (payload.actionReport) {
            case 'REG_SUCCESS':
                udText = 'R';
                console.log('[helpers][operator][ais][ussd]: Incoming PAyload is for Registration:');
                break;
            case 'UNREG_IMMEDIATE':
                udText = 'C';
                console.log('[helpers][operator][ais][ussd]: Incoming PAyload is for Cancelling Registration:');
                break;
            default:
                udText = payload.actionReport;
        }


        // Try to Register...
        let queryData = {
            query_name: "ais_register_service_ussd",
            operator_rid: payload.operatorId,
            channel_rid: payload.channelId,
            partner_rid: payload.operatorId,            // Same as operator rid
            destination_address_number: payload.sender,
            ud: udText,
            source_address_number: payload.msisdn,
            original_message_json: payload,
            ussd_code: '',
            short_code: payload.sender,
            service_id: payload.sender,
            ais_linked_id: payload.linkedId,
            ais_recipient: payload.recipient
        };

        console.log(`[helpers][operator][ais][ussd]: Prepared Query Data (SMS):`, JSON.stringify(queryData));


        let data = await execDbProcedure(queryData);
        // console.log('[helpers][operator][ais][ussd]: Data From Query:', JSON.stringify(data));

        data = data.return_value && data.return_value[0] ? data.return_value[0] : data.return_value;

        if (!data) {
            console.error(`[helpers][operator][ais][ussd]: Query Result From DB gives Return Value Null Hence cannot process it further`);
            throw new Error(`Query Result From DB gives Return Value Null Hence cannot process it further: ${JSON.stringify(data)}`);
        }

        console.log('[helpers][operator][ais][ussd]: Return Value:', JSON.stringify(data));


        const url = data.request_mt_notification.request_register_ip;

        console.log('[helpers][operator][ais][ussd]: Operator Endpoint (Ip):', url);
        console.log('[helpers][operator][ais][ussd]: Response Type From DB Call:', data.response_type);


        if (url) {
            if (data.response_type == 'response_on_register') {

                await respondToOperator(data, url);

            } else if (data.response_type == 'duplicate_registration') {

                await respondToOperator(data, url);

            } else if (data.response_type == 'wrong_format') {

                await respondToOperator(data, url);

            } else if (data.response_type == 'response_on_cancel' || 'duplicate_cancellation') {

                await respondToOperator(data, url);

            } else {
                console.log('[helpers][operator][ais][ussd]: Unknow Response Type From DB Call:', data.response_type);
            }

        } else {
            console.warn(`[helpers][operator][ais][ussd]: Operator Endpoint is null, Hence Aborting, Please check setup`);
        }

        
        // Save Incoming Request To Log...
        queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: data.registration_transaction_id,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "I",
            request_json: {
                ...payload,
                requestRawBody: requestRawBody
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                ...data,
                responseRawBody: responseRawBody
            },
            response_timestamp: new Date().toISOString(),
            message_info: ""   // notify_user or notify_operator In case of outgoing
        };

        console.log(`[helpers][operator][ais][ussd]: Saving Incoming Request Log:`, queryData);
        await execDbProcedure(queryData);

        return data;           

    } catch (err) {
        console.error('[helpers][operator][true][ussd]:  Error:', err);
        throw new Error(`${err.message}`);
    }

};

module.exports = processUSSDRequest;
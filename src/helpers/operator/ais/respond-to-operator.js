const axios = require('../../../helpers/axios');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');

const randomString = (length) => {
    return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}


const respondToOperator = async (data, url) => {

    const timestamp = new Date().getTime();
    const msg = `${data.request_mt_notification.request_text}`;
    const base64UrlEncodedMsg = new Buffer.from(msg).toString('base64');
    console.log('[helpers][operator][ais][respond-to-operator]: Text Msg/ base64UrlEncodedMsg:', msg, base64UrlEncodedMsg);

    const bodyBoundary = randomString(12);
    console.log('[helpers][operator][ais][respond-to-operator]: bodyBoundary:', bodyBoundary);

    // // Try to Register...
    // let queryData = {
    //     query_name: "ais_get_dummy_values"
    // };
    // console.log(`[helpers][operator][ais][respond-to-operator]: Prepared Query Data:`, queryData);

    // let dbResponse = await execDbProcedure(queryData);
    // console.log('[helpers][operator][ais][respond-to-operator]: Data From Query:', JSON.stringify(dbResponse));
    // dbResponse = dbResponse.return_value && dbResponse.return_value[0] ? dbResponse.return_value[0] : dbResponse.return_value;

    const basicAuth = new Buffer.from(`${data.request_mt_notification.request_username}:${data.request_mt_notification.request_password}`).toString('base64');
    console.log('[helpers][operator][ais][respond-to-operator]: basicAuth:', basicAuth);


    const cctValue = data.request_mt_notification.ais_notification_cct;
    const cpActionValue = data.request_mt_notification.ais_notification_cpaction;
    const senderAddress = '' + data.request_mt_notification.request_register_short_code;
    const senderName = '' + data.request_mt_notification.request_sender_name;

    let vaspId = data.ais_specific_parameters.vaspid;
    let vasId = data.ais_specific_parameters.vasid;
    console.log('[helpers][operator][ais][respond-to-operator]: VASP ID/ Vas ID:', vaspId, vasId);

    let messageSubject = 'Thankyou Message';

    const requestBody = `------=_Part_1_${bodyBoundary}.${timestamp}\r\nContent-Type: text/xml; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\nContent-ID: <rootpart>\r\n\r\n<?xml version="1.0" encoding="UTF-8"?>\r\n<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">\r\n   <soapenv:Header>\r\n      <TransactionID xmlns="http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0">${new Date().getTime()}</TransactionID>\r\n   </soapenv:Header>\r\n   <soapenv:Body xmlns:ns3="http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0">\r\n      <ns3:SubmitReq xmlns="http://www.3gpp.org/ftp/Specs/archive/23_series/23.140/schema/REL-5-MM7-1-0">\r\n         <ns3:MM7Version>5.3.0</ns3:MM7Version>\r\n         <ns3:SenderIdentification>\r\n            <ns3:VASPID>${vaspId}</ns3:VASPID>\r\n            <ns3:VASID>${vasId}</ns3:VASID>\r\n            <ns3:SenderAddress>\r\n               <ns3:Number>${senderAddress}</ns3:Number>\r\n            </ns3:SenderAddress>\r\n         </ns3:SenderIdentification>\r\n         <ns3:Recipients>\r\n            <ns3:To>\r\n               <ns3:Number>${data.ais_specific_parameters.recipient}</ns3:Number>\r\n            </ns3:To>\r\n         </ns3:Recipients>\r\n        <PreferredChannels>\r\n                <DeliverUsing>SMS</DeliverUsing>\r\n        </PreferredChannels>\r\n            <MessageExtraData>\r\n                <element>\r\n                    <key>AISVersion</key>\r\n                    <value>1.0</value>\r\n                </element>\r\n                <element>\r\n                    <key>CCT</key>\r\n                    <value>${cctValue}</value>\r\n                </element>\r\n                <element>\r\n                    <key>UserDataEncoding</key>\r\n                    <value>unicode</value>\r\n                </element>\r\n                <element>\r\n                    <key>sender_name</key>\r\n                    <value>${senderName}</value>\r\n                </element>\r\n                <element>\r\n                    <key>CPName</key>\r\n                    <value>${senderAddress}</value>\r\n                </element>\r\n                <element>\r\n                    <key>CPAction</key>\r\n                    <value>${cpActionValue}</value>\r\n                </element>\r\n                <element>\r\n                    <key>SMSText</key>\r\n                    <value>${msg}</value>\r\n                </element>\r\n            </MessageExtraData>\r\n            <ServiceCode>09</ServiceCode>\r\n            <LinkedID>${data.ais_specific_parameters.linked_id}</LinkedID>\r\n            <EarliestDeliveryTime>PT0S</EarliestDeliveryTime>\r\n            <ExpiryDate>PT10M</ExpiryDate>\r\n         <ns3:DeliveryReport>true</ns3:DeliveryReport>\r\n         <ns3:Subject>${messageSubject}</ns3:Subject>\r\n         <ns3:Content href="cid:attachment1.txt" />\r\n      </ns3:SubmitReq>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>\r\n------=_Part_1_${bodyBoundary}.${timestamp}\r\nContent-Type: text/plain; charset=UTF-8; name=attachment1.txt\r\nContent-Transfer-Encoding: base64\r\nContent-Disposition: attachment; filename=attachment1.txt\r\nContent-ID: <attachment1.txt>\r\n\r\n${base64UrlEncodedMsg}\r\n------=_Part_1_${bodyBoundary}.${timestamp}--`;

    console.log('[helpers][operator][ais][respond-to-operator]: URL:', url);
    console.log('[helpers][operator][ais][respond-to-operator]: Sending Thankyou Message Body:', requestBody);
    
    try {

        let axiosResponse = await axios(
            {
                method: 'post',
                url: url,
                headers: { 
                    'Content-Type': `multipart/related; type="text/xml"; start="<rootpart>"; boundary="----=_Part_1_${bodyBoundary}.${timestamp}"`, 
                    'Authorization': `Basic ${basicAuth}`
                  },
                data : requestBody
              }
        );

        console.log('[helpers][operator][ais][respond-to-operator]: Send Operation Status to Operator Successfully:', axiosResponse.data);

        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: requestBody
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                responseRawBody: axiosResponse.data
            },
            response_timestamp: new Date().toISOString(),
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][ais][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

    } catch (err) {

        if (err.response) {
            // client received an error response (5xx, 4xx)
            console.error('[helpers][operator][ais][respond-to-operator]: Failed to Send Message to Operator: Recieved Error Response:', err.message);
        } else if (err.request) {
            // client never received a response, or request never left
            console.error('[helpers][operator][ais][respond-to-operator]: Failed to Send Message to Operator: No Response:', err.message);
        } else {
            // anything else
            console.error('[helpers][operator][ais][respond-to-operator]: Some other Error in Sending Message to Operator:', err);
        }


        let queryData = {
            query_name: "member_activity_log_req_res_save",
            record_id: null,
            member_activity_log_rid: null,
            registration_transaction_id: data.registration_transaction_id,
            in_out: "O",
            request_json: {
                ...data,
                requestRawBody: requestBody
            },
            request_timestamp: new Date().toISOString(),
            response_json: {
                responseRawBody: err
            },
            response_timestamp: new Date().toISOString(),
            message_info: "notify_operator"   // notify_user or notify_operator
        };

        console.log(`[helpers][operator][ais][respond-to-operator]: Saving Response Data:`, queryData);
        await execDbProcedure(queryData);

        throw new Error(`Failed to notify Operator about operation status: ${err.message}`);

    }
    
};

module.exports = respondToOperator;
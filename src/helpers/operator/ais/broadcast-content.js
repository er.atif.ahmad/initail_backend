const axios = require('../../../helpers/axios');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const utilityHelper = require('../../../helpers/utility');
const sendToSQS = require('../../../helpers/queue');
const s3Uploader = require('../../../helpers/s3Helper');
const Parallel = require('async-parallel');
const Client = require('ssh2-sftp-client');
const path = require('path');
const fs = require('fs-extra');
const iconv = require('iconv-lite');


const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);


const putFileOnSftp = async (host, username, broadcastFilePath) => {
    console.log('IP:', host);
    console.log('User:', username);

    const pemFile = path.join(global.appRoot, 'key-files', 'ais', 'grammy_rsa');
    console.log('Pem File Path:', pemFile);
    const pemKey = fs.readFileSync(pemFile, 'utf8');
    // console.log('Pem Key:', pemKey);

    // Fetch file from S3 to broadcast..............
    const localFileDir = path.join(global.appRoot, 'tmp', 'ais', 'broadcast-files', 'downloaded');
    fs.ensureDirSync(localFileDir);

    let filename = broadcastFilePath.split('/').pop();
    console.log('Downloaded Broadcast File Name:', filename);
    let localFilePath = path.join(localFileDir, filename);

    await s3Uploader.downloadFileToLocal(broadcastFilePath, localFilePath);
    console.log('Downloaded Broadcast File Successfully...');

    content = await fs.readFile(localFilePath);
    console.log('Content From Downloaded Broadcast File:', content);

    content = iconv.decode(Buffer.from(content), 'utf-8');   // iconv -f   // from-encoding
    content = iconv.encode(content, 'ISO885911');            // iconv -t   // to-encoding
    
    console.log('Content After encoding:', content);
    
    await fs.writeFile(localFilePath, content);

    let sftp = new Client();
    try {

        if (process.env.APP_ENV == 'local') {
            console.log('Skipping SFTP FIle Upload on Local ENv.');
        } else {
            await sftp.connect({
                host: host,
                port: 22,
                username: username,
                privateKey: pemKey
            });

            let res = await sftp.fastPut(localFilePath, `./data/${filename}`);
            console.log('File Upload Res:', res);
        }

        return localFilePath;

    } catch (err) {
        console.error('Could Not Upload File Error:', err);
        throw err;
    }

};


const respondToOperator = async (message) => {

    console.log(`[helpers][ais][broadcast-content]: Message for Broadcast:`, JSON.stringify(message));

    let contentBroadcasted = false;
    let { members, broadcastFilePath, operator } = JSON.parse(JSON.stringify(message));

    console.log(`[helpers][ais][broadcast-content]: Members:`, JSON.stringify(members));
    console.log(`[helpers][ais][broadcast-content]: Broadcast FilePath:`, broadcastFilePath);
    console.log(`[helpers][ais][broadcast-content]: Operator:`, operator);

    try {

        const localFilePath = await putFileOnSftp(operator.ip_address, operator.username, broadcastFilePath)
        console.log(`[helpers][ais][broadcast-content]: Broadcasted File Put Successfully:`, broadcastFilePath);

        let broadcastContent = fs.readFileSync(localFilePath, 'utf8')
        console.log(`[helpers][ais][broadcast-content]: Content Broadcasted :`, broadcastContent);

        // Now update that this message is broadcasted.. & also do save the logs
        let recordset = [];
        let logRecordset = [];
        let filename = broadcastFilePath.split('/').pop();
        let gMessageId = filename.toString().slice(0,-4);
        
        members.forEach(member => {
            let r = {
                record_id: member.record_id,
                updates: [
                    { field_name: "on_queue_flag", field_value: 2 },
                    { field_name: "success_sent_flag", field_value: 1 },
                ]
            };
            recordset.push(r);

            r = {
                record_id: null,
                broadcast_content_rounds_service_member_rid: member.record_id,
                member_rid: member.member_rid,
                server_log_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
                incoming_outgoing_flag: "O",
                request_message: broadcastContent,
                response_message: '',
                "internal_message_id": null,
                "external_message_id": gMessageId,
                "message_type": null,
                "message_status": null,
                "message_code": null,
                "message_description": null,
                "operator_rid": operator.operator_rid
            } 
            logRecordset.push(r);
        });

        let queryData = {
            query_name: "round_member_list_update_process_bulk",
            recordset: recordset
        };
        await execDbProcedure(queryData);

        queryData = {
            query_name: "broadcast_content_rounds_service_member_log_bulk_save",
            recordset: logRecordset
        };
        await execDbProcedure(queryData);

        console.log('[helpers][ais][broadcast-content]: Bulk Log Save done for success broadcast');

        contentBroadcasted = true;

    } catch (err) {

        console.error('[helpers][ais][broadcast-content]: Some Error in putting broadcast file on SFTP:', err);

        let recordset = [];
        members.forEach(member => {
            let r = {
                record_id: member.record_id,
                updates: [
                    { field_name: "resent_flag", field_value: 1 }
                ]
            };
            recordset.push(r);
        });

        let queryData = {
            query_name: "round_member_list_update_process_bulk",
            recordset: recordset
        };
        await execDbProcedure(queryData);

    }

    return contentBroadcasted;

};

module.exports = respondToOperator;
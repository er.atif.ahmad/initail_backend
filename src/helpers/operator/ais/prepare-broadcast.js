const Parallel = require('async-parallel');
const execDbProcedure = require('../../../helpers/db/exec-db-procedure');
const sendToSQS = require('../../../helpers/queue');
const s3Uploader = require('../../../helpers/s3Helper');
const path = require('path');
const fs = require('fs-extra');

const moment = require('moment-timezone');
const timezone = 'Asia/Bangkok';
moment.tz.setDefault(timezone);
const delay = ms => new Promise(res => setTimeout(res, ms));

const createFileToPut = async (broadcastContent, formattedTimestamp, members, senderId, broadcastRoundId) => {

    broadcastContent = broadcastContent.replace("__XX_FORMATTED_TS_XX__", formattedTimestamp);
    let content = broadcastContent;

    const membersLength = members.length;

    members.forEach((mem, i) => {

        if (membersLength - 1 == i) {
            content = content.replace("66XX_MOBILE_NUMBERS_XX99",
                `${mem.msisdn}`
            );
        } else {
            content = content.replace("66XX_MOBILE_NUMBERS_XX99",
                `${mem.msisdn}\r\n66XX_MOBILE_NUMBERS_XX99`
            );
        }
    });

    const dirPath = path.join(global.appRoot, 'tmp', 'ais', 'broadcast-files', '' + broadcastRoundId);
    fs.ensureDirSync(dirPath);

    const filePath = path.join(dirPath, `${senderId}_${formattedTimestamp}.txt`);
    console.log('FilePath:', filePath);

    fs.writeFileSync(filePath, content);

    // Now upload file to S3, and return s3 url...
    let s3FilePath = `operators/ais/broadcast-files/${senderId}/${formattedTimestamp}/${senderId}_${formattedTimestamp}.txt`;

    let s3UploadRes = await s3Uploader.uploadLocalFile(filePath, s3FilePath);
    console.log('[helpers][ais][prepareBroadcast][createFileToPut]: S3 Upload Response:', JSON.stringify(s3UploadRes));

    return s3FilePath;
}



const prepareBroadcast = async (round, operator) => {

    try {

        console.log('[helpers][ais][prepareBroadcast]: Round Data:', JSON.stringify(round));
        console.log('[helpers][ais][prepareBroadcast]:: Operator :', JSON.stringify(operator));

        const broadcastTime = moment(round.broadcast_datetime);
        let diffInSeconds = moment.duration(broadcastTime.diff(moment()));
        diffInSeconds = diffInSeconds.asSeconds();

        diffInSeconds = parseInt(diffInSeconds);
        console.log('[helpers][ais][prepareBroadcast]: Diff In Seconds / Broadcast Time: ', diffInSeconds, round.broadcast_datetime);
        if (diffInSeconds < 1) {
            diffInSeconds = 0;
        }

        let orignalBroadcastContent = operator.broadcastContent;
        delete operator.broadcastContent;

        let queryData = {
            query_name: "round_member_list_prepare",
            service_round_rid: round.service_round_rid,
            operator_rid: operator.operator_rid
        };

        let memberPrepareResult = await execDbProcedure(queryData);
        memberPrepareResult = memberPrepareResult.return_value[0] ? memberPrepareResult.return_value[0] : memberPrepareResult.return_value;
        if (!memberPrepareResult) {
            console.error('[helpers][ais][prepareBroadcast]: Return Value Error:', memberPrepareResult);
            throw new Error(`Unknown Return Value: ${memberPrepareResult}`);
        }
        let totalRecordsGenerated = memberPrepareResult.total_records;
        console.warn('[helpers][ais][prepareBroadcast]: Total Records Generated on (round_member_list_prepare):', totalRecordsGenerated);
        // IF no records then no need to continue...
        if (!totalRecordsGenerated || totalRecordsGenerated < 1) {
            console.warn('[helpers][ais][prepareBroadcast]: No Member subscribers for :', memberPrepareResult, round, operator);
            return;
        }

        const recordsPerIteration = 999;
        const memberFetchQueries = [];

        for (let i = 0; i < Math.ceil(totalRecordsGenerated / recordsPerIteration); i++) {
            queryData = {
                query_name: "round_member_list_get",
                service_round_rid: round.service_round_rid,
                operator_rid: operator.operator_rid,
                from_row_num: (recordsPerIteration * i) + 1,
                to_row_num: (recordsPerIteration * i) + recordsPerIteration
            };
            memberFetchQueries.push(queryData);
        }

        Parallel.setConcurrency(1);
        let allMembers = [];
        await Parallel.each(memberFetchQueries, async (q) => {
            let members = await execDbProcedure(q);
            members = members.return_value;
            console.log('[helpers][ais][prepareBroadcast]: Members for operator/round :', operator.operator_rid, round.service_round_rid, q);

            members = members.map(m => {
                return {
                    record_id: m.record_id,
                    member_rid: m.member_rid,
                    msisdn: m.msisdn
                };
            });

            // Now add the logic to create broadcast file with members and put it somewhere may be on s3
            const formattedTimestamp = moment().add(diffInSeconds, 'seconds').add(5, 'minutes').format('YYYYMMDDHHmmss');
            const s3FilePath = await createFileToPut(orignalBroadcastContent, formattedTimestamp, members, round.operator_list[0].broadcast_sender_name, round.service_round_rid);

            let sqsPayload = {
                members,
                broadcastFilePath: s3FilePath,
                operator,
                operatorId: operator.operator_rid
            };

            let sqsMessageAttributes = {
                "createdAt": {
                    DataType: "String",
                    StringValue: '' + moment().valueOf()
                },
                "messageType": {
                    DataType: "String",
                    StringValue: 'BROADCAST_MESSAGE_CONTENT'
                }
            };
            let sqsPublishResponse = await sendToSQS(sqsPayload, sqsMessageAttributes, diffInSeconds);
            console.log('[helpers][ais][prepareBroadcast]: SQS Publish Response /Delivery delay:', sqsPublishResponse, diffInSeconds);
            let memberRids = members.map(m => m.record_id);
            allMembers = [...allMembers, ...memberRids];
            console.log('[helpers][ais][prepareBroadcast]: Messages pushed for members: ', JSON.stringify(memberRids));
            await delay(1000);
        });
        Parallel.setConcurrency(20);


        // Now update that this message on queue
        let recordset = [];
        allMembers.forEach(memberRid => {
            let r = {
                record_id: memberRid,
                updates: [
                    { field_name: "on_queue_flag", field_value: 1 }
                ]
            };
            recordset.push(r);
        });

        queryData = {
            query_name: "round_member_list_update_process_bulk",
            recordset: recordset
        };
        await execDbProcedure(queryData);



        // Call DB Proc to inform that these rounds are on queue for broadcasting...
        queryData = {
            query_name: "prepare_daily_broadcast_update_process",
            updates: [
                { service_round_rid: round.service_round_rid, "process_status": 2 },
            ]
        };
        await execDbProcedure(queryData);
        console.log('[helpers][ais][prepareBroadcast]: Updated Process Status to 2 in DB:');

    } catch (err) {
        console.error('[helpers][ais][prepareBroadcast]: Some Error in putting up to broadcast queue:', err);

        for (var item of err.list) {
            console.log('[helpers][ais][prepareBroadcast]: Each Error1: ', item.message);
        }
    }

};

module.exports = prepareBroadcast;
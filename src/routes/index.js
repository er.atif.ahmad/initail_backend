const express = require('express');
const router = express.Router();
const v1 = require('./v1/index');
const test = require('./test/index');


/* GET home page. */
router.get('/', async (req, res, next) => {
  // console.log(process.env);
  res.json({app: "serverless Express App"});
});


/**
 * Routers
 */

router.use(process.env.SUFIX_URI+'/api/v1', v1);
router.use(process.env.SUFIX_URI+'/api/test', test);




module.exports = router;

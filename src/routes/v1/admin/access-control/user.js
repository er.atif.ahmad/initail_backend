const express = require('express');
const router = express.Router();

const userController = require('../../../../controllers/v1/superadmin/access-control/user');
const authMiddleware = require('../../../../middlewares/auth');
// const roleMiddleware = require('../../../../middlewares/role');
// const resourceAccessMiddleware = require('../../../../middlewares/resource-access');




/* GET users listing. */
// router.post('/', authMiddleware.isAuthenticated, roleMiddleware.parseUserPermission, resourceAccessMiddleware.isAccessControlAccessible,  userController.list);

router.post('/', authMiddleware.isAuthenticated, userController.list);
router.post('/create', authMiddleware.isAuthenticated, userController.createUser);

router.get('/:id', authMiddleware.isAuthenticated, userController.userDetail);
router.put('/:id', authMiddleware.isAuthenticated, userController.updateUser);
router.patch('/:id', authMiddleware.isAuthenticated, userController.updateUserStatus);

module.exports = router;

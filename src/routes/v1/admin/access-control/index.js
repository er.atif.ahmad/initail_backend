var express = require('express');
var router = express.Router();
const user = require('./user');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a access-control');
});

router.use('/users', user);

module.exports = router;

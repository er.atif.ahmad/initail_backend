const express = require('express');
const router = express.Router();

const masterController = require('../../../controllers/v1/superadmin/master');


/* GET users listing. */
router.post('/language', masterController.listActiveLanguage);


module.exports = router;

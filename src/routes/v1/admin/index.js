const express = require('express');
const router = express.Router();
const accessControl = require('./access-control/index');
const masterControl = require('./master');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource superadmin');
});

router.use('/access-control', accessControl);
router.use('/master', masterControl);

module.exports = router;

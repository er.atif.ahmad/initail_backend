const express = require('express');
const router = express.Router();

const roleController = require('../../../../controllers/v1/superadmin/access-control/role');
const authMiddleware = require('../../../../middlewares/auth');
// const roleMiddleware = require('../../../../middlewares/role');
// const resourceAccessMiddleware = require('../../../../middlewares/resource-access');


/* GET users listing. */
// router.post('/', authMiddleware.isAuthenticated, roleMiddleware.parseUserPermission, resourceAccessMiddleware.isAccessControlAccessible, roleController.listRoles);
// router.post('/create', authMiddleware.isAuthenticated, roleMiddleware.parseUserPermission, resourceAccessMiddleware.isAccessControlAccessible, roleController.createRole);
// router.get('/:id', authMiddleware.isAuthenticated, roleMiddleware.parseUserPermission, resourceAccessMiddleware.isAccessControlAccessible, roleController.roleDetail);
// router.put('/:id', authMiddleware.isAuthenticated, roleMiddleware.parseUserPermission, resourceAccessMiddleware.isAccessControlAccessible, roleController.updateRole);

router.post('/', authMiddleware.isAuthenticated, roleController.listRoles);
router.post('/create', authMiddleware.isAuthenticated, roleController.createRole);
router.get('/:id', authMiddleware.isAuthenticated, roleController.roleDetail);
router.put('/:id', authMiddleware.isAuthenticated, roleController.updateRole);
router.patch('/:id', authMiddleware.isAuthenticated, roleController.updateRoleStatus);


module.exports = router;

const express = require('express');
const router = express.Router();
const users = require('./users/index');
const admin = require('./admin/index');
const superadmin = require('./superadmin/index');
const entranceControl = require('./entrance');


/* GET home page. */
router.get('/', async (req, res, next) => {
  // console.log(process.env);
  res.json({app: "serverless Express App v1"});
});


/**
 * Routers
 */

router.use('/users', users);
router.use('/admin', admin);
router.use('/superadmin', superadmin);
router.use('/entrance', entranceControl);




module.exports = router;

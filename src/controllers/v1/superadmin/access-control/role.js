const knex = require('../../../../db/knex');
const Joi = require('joi');
const _ = require('lodash');
const moment = require("moment-timezone");


const roleController = {

    roleDetail: async (req, res) => {

        let id = req.params.id;

        console.log("=== me ===", req.me);

        try {

            if (!id) {
                res.status(400).json({
                    errors: [
                        { code: 'BAD_REQUEST', message: 'Invalid Role Id' }
                    ],
                });
            }

            const applicationRole = await knex('application_roles').where({id}).first();
            console.log(`[controllers][v1][role][roleDetail]: Role Details:`, applicationRole);

            
            return res.status(200).json({ data: applicationRole });

        } catch (err) {
            console.log('[controllers][v1][role][roleDetail] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    listRoles: async (req, res) => {

        try {

            console.log('[controllers][role][listRoles]');

            let reqData = req.query;

            console.log("=== me ===", req.me);

            //let filters = {}
            let total, rows;
            let { filter, sortFilter } = req.body;
            let pagination = {};
            let per_page = reqData.per_page || 10;
            let page = reqData.current_page || 1;
            if (page < 1) page = 1;
            let offset = (page - 1) * per_page;

            [total, rows] = await Promise.all([
                knex.count("* as count")
                    .from("application_roles as ar")
                    .where((qb) => {
                        if(filter){
                            if(filter?.code && filter?.code !=''){
                                qb.where(
                                    "ar.code",
                                    filter.code
                                );
                            }
                        }
                    })
                    .first(),

                knex("application_roles as ar")
                    .select(['*'])
                    .where((qb) => {
                        if(filter){
                            if(filter?.code && filter?.code !=''){
                                qb.where(
                                    "ar.code",
                                    filter.code
                                );
                            }
                        }
                    })
                    .orderBy("ar.id", "desc")
                    .offset(offset)
                    .limit(per_page)
            ]);

            let count = total.count;
            pagination.total = count;
            pagination.per_page = per_page;
            pagination.offset = offset;
            pagination.to = offset + rows.length;
            pagination.last_page = Math.ceil(count / per_page);
            pagination.current_page = page;
            pagination.from = offset;
            pagination.data = rows;

            return res.status(200).json(pagination);

        } catch (err) {
            console.log('[controllers][role][listRoles] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    createRole: async (req, res) => {

        try {

            const payload = req.body;
            console.log('[controllers][role][createRole]:', payload);

            console.log("=== me ===", req.me);
            
            const schema = Joi.object().keys({
                name: Joi.string().required().min(1),
                code: Joi.string().required().min(1).max(255),
                isActive: Joi.boolean().required(),
            });

            const result = schema.validate(payload);
            console.log('[controllers][role][createRole]: Joi Validate Result', result);

            if (result && result.hasOwnProperty('error') && result.error) {
                return res.status(400).json({
                    errors: [
                        { code: 'VALIDATION_ERROR', message: result.error.message }
                    ],
                });
            }

            let currentTime = moment().valueOf();

            await knex.transaction(async trx => {
                let addedAppRole = await knex("application_roles")
                    .insert({ name: payload.name, code: payload.code, isActive: payload.isActive, createdBy: req.me.id, createdAt: currentTime })
                    .returning(["*"])
                    .transacting(trx);
            });

            console.log('[controllers][role][createRole]: Payload:', payload);

            return res.status(200).json({ data: {}, message: 'Role Created Successfully.' });

        } catch (err) {
            console.log('[controllers][role][createRole] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    updateRole: async (req, res) => {

        try {

            let id = req.params.id;

            console.log("=== me ===", req.me);

            if (!id) {
                res.status(400).json({
                    errors: [
                        { code: 'BAD_REQUEST', message: 'Invalid Role Id' }
                    ],
                });
            }

            const payload = req.body;
            console.log('[controllers][v1][role][updateRole]:', payload);

            const schema = Joi.object().keys({
                name: Joi.string().required().min(1),
                code: Joi.string().required().min(1).max(255),
                isActive: Joi.boolean().required()
            });

            const result = schema.validate(payload);
            console.log('[controllers][role][createRole]: Joi Validate Result', result);

            if (result && result.hasOwnProperty('error') && result.error) {
                return res.status(400).json({
                    errors: [
                        { code: 'VALIDATION_ERROR', message: result.error.message }
                    ],
                });
            }

            let currentTime = moment().valueOf();

            await knex.transaction(async trx => {
                let updatedAppRole = await knex("application_roles")
                    .update({ name: payload.name, code: payload.code, isActive: payload.isActive, updatedBy:req.me.id, updatedAt: currentTime })
                    .where({id})
                    .returning(["*"])
                    .transacting(trx);
            });

            console.log('[controllers][v1][role][updateRole]: Payload:', payload);

            return res.status(200).json({ data: {}, message: 'Role Updated Successfully.' });

        } catch (err) {
            console.log('[controllers][v1][role][updateRole]:  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    updateRoleStatus: async (req, res) => {

        try {

            let id = req.params.id;

            console.log("=== me ===", req.me);

            if (!id) {
                res.status(400).json({
                    errors: [
                        { code: 'BAD_REQUEST', message: 'Invalid User Id' }
                    ],
                });
            }

            const payload = req.body;
            console.log('[controllers][v1][v1][user][updateUserStatus]:', payload);

            const schema = Joi.object().keys({
                isActive: Joi.boolean().required(),
            });

            const result = schema.validate(payload);
            console.log('[controllers][v1][user][updateUserStatus]: Joi Validate Result', result);

            if (result && result.hasOwnProperty('error') && result.error) {
                return res.status(400).json({
                    errors: [
                        { code: 'VALIDATION_ERROR', message: result.error.message }
                    ],
                });
            }

            let currentTime = moment().valueOf();

            await knex.transaction(async trx => {
                let updatedRoles = await knex("application_roles")
                    .update({ ...payload, updatedBy:req.me.id, updatedAt: currentTime })
                    .where({id})
                    .returning(["*"])
                    .transacting(trx);

            });

            console.log('[controllers][v1][v1][role][updateRoleStatus]: Payload:', payload);

            return res.status(200).json({ data: {}, message: 'Role Status Updated Successfully.' });

        } catch (err) {
            console.log('[controllers][v1][v1][role][updateRoleStatus]:  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },
};

module.exports = roleController;
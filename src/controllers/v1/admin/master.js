const knex = require('../../../db/knex');
const Joi = require('joi');
const _ = require('lodash');
const moment = require("moment-timezone");


const masterController = {

    listActiveLanguage: async (req, res) => {

        try {

            console.log('[controllers][master][listActiveLanguage]');

            let reqData = req.query;

            //let filters = {}
            let total, rows;
            let { filter, sortFilter } = req.body;
            let pagination = {};
            let per_page = reqData.per_page || 100;
            let page = reqData.current_page || 1;
            if (page < 1) page = 1;
            let offset = (page - 1) * per_page;

            [total, rows] = await Promise.all([
                knex.count("* as count")
                    .from("language as l")
                    .where((qb) => {
                        if(filter){
                            if(filter?.name && filter?.name !=''){
                                qb.where(
                                    "l.name",
                                    filter.name
                                );
                            }

                            if(filter?.code && filter?.code !=''){
                                qb.where(
                                    "l.code",
                                    filter.code
                                );
                            }
                        }
                    })
                    .where('l.isActive', true)
                    .first(),

                knex("language as l")
                    .select(['*'])
                    .where((qb) => {
                        if(filter){
                            if(filter?.name && filter?.name !=''){
                                qb.where(
                                    "l.name",
                                    filter.name
                                );
                            }

                            if(filter?.code && filter?.code !=''){
                                qb.where(
                                    "l.code",
                                    filter.code
                                );
                            }
                        }
                    })
                    .where('l.isActive', true)
                    .orderBy("l.id", "asc")
                    .offset(offset)
                    .limit(per_page)
            ]);

            let count = total.count;
            pagination.total = count;
            pagination.per_page = per_page;
            pagination.offset = offset;
            pagination.to = offset + rows.length;
            pagination.last_page = Math.ceil(count / per_page);
            pagination.current_page = page;
            pagination.from = offset;
            pagination.data = rows;

            return res.status(200).json(pagination);

        } catch (err) {
            console.log('[controllers][master][listActiveLanguage] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },
};

module.exports = masterController;
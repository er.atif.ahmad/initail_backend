const knex = require('../../../../db/knex');
const Joi = require('joi');
const bcrypt = require('bcryptjs');
const saltRounds = 10;
const { v4: uuidv4 } = require('uuid');
var jwt = require('jsonwebtoken');
const _ = require('lodash');
// const redisHelper = require('../../../helpers/redis');
const moment = require("moment-timezone");


const userController = {

    userDetail: async (req, res) => {

        let id = req.params.id;

        console.log("=== me ===", req.me);

        try {

            if (!id) {
                res.status(400).json({
                    errors: [
                        { code: 'BAD_REQUEST', message: 'Invalid User Id' }
                    ],
                });
            }

            const usersDetails = await knex
            .select('u.*', 'ar.name as roleName')
            .from('users as u')
            .leftJoin(
                "application_roles as ar",
                "ar.id",
                "u.roleId"
                )
            .where("u.roleId",3)
            .where("u.id",id)
            .where("u.shopId", req.me.id)
            .first();
            console.log(`[controllers][v1][v1][user][usersDetail]: Users Details:`, usersDetails);

            // let resources = await knex.raw(`select "resourceId", jsonb_array_elements(jsonb_agg("accessType")) as permissions 
            //                                     from role_resource_access_master where "roleId" = ${id} group by "resourceId";`);

            // resources = resources.rows;                                    

            // const Parallel = require('async-parallel');
            // resources = await Parallel.map(resources, async (res) => {
            //     let resource = await knex.from('resources').select(['id', 'resourceName', 'resourceKey'])
            //         .where({ id: res.resourceId })
            //         .first();

            //     res = { ...res, ...resource };
            //     return res;
            // });
            // applicationRole.resources = resources;
            return res.status(200).json({ data: usersDetails });

        } catch (err) {
            console.log('[controllers][v1][v1][user][usersDetail] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    list: async (req, res) => {

        try {

            console.log('[controllers][v1][user][list]');

            let reqData = req.query;

            console.log("=== me ===", req.me);

            //let filters = {}
            let total, rows;
            let { filter, sortFilter } = req.body;
            //console.log("=== filter ===", filter);
            let pagination = {};
            let per_page = reqData.per_page || 10;
            let page = reqData.current_page || 1;
            if (page < 1) page = 1;
            let offset = (page - 1) * per_page;

            [total, rows] = await Promise.all([
                knex.count("u.id as count")
                    .from("users as u")
                    .leftJoin(
                        "application_roles as ar",
                        "ar.id",
                        "u.roleId"
                        )
                    .where("u.roleId",3)
                    .where("u.shopId", req.me.id)
                    .where((qb) => {
                        if(filter){
                            if(filter?.name && filter?.name !=''){
                                qb.where(
                                    "u.name",
                                    filter.name
                                );
                            }
                            if(filter?.email && filter?.email !=''){
                                qb.where(
                                    "u.email",
                                    filter.email
                                );
                            }
                        }
                    })
                    .first(),

                knex("users as u")
                    .select(['u.*'])
                    .leftJoin(
                        "application_roles as ar",
                        "ar.id",
                        "u.roleId"
                        )
                    .where("u.roleId",3)
                    .where("u.shopId", req.me.id)
                    .where((qb) => {
                        if(filter){
                            if(filter?.name && filter?.name !=''){
                                qb.where(
                                    "u.name",
                                    filter.name
                                );
                            }
                            if(filter?.email && filter?.email !=''){
                                qb.where(
                                    "u.email",
                                    filter.email
                                );
                            }
                        }
                    })
                    .orderBy("u.id", "desc")
                    .offset(offset)
                    .limit(per_page)
            ]);

            let count = total.count;
            pagination.total = count;
            pagination.per_page = per_page;
            pagination.offset = offset;
            pagination.to = offset + rows.length;
            pagination.last_page = Math.ceil(count / per_page);
            pagination.current_page = page;
            pagination.from = offset;
            pagination.data = rows;

            // const Parallel = require('async-parallel');

            // pagination.data = await Parallel.map(rows, async pd => {

            //     let assocResPermissions = await knex.from('role_resource_access_master as rram')
            //         .join('resources as rs', 'rs.id', 'rram.resourceId')
            //         .where({ "rram.roleId": pd.id })
            //         .select(['rram.resourceId', 'rram.accessType as permissions', 'rs.resourceName']);

            //     return {
            //         ...pd,
            //         resources: assocResPermissions,
            //     };

            // });
            return res.status(200).json(pagination);

        } catch (err) {
            console.log('[controllers][v1][user][list] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    createUser: async (req, res) => {

        try {

            const payload = req.body;
            console.log('[controllers][v1][user][createUser]:', payload);

            const schema = Joi.object().keys({
                name: Joi.string().required().min(1).max(255),
                userName: Joi.string().required().min(1).max(255),
                email: Joi.string().required().min(1).max(500),
                mobileNo: Joi.string().required().min(1).max(20),
                password: Joi.string().required().min(1).max(50)
            });

            const result = schema.validate(payload);
            console.log('[controllers][v1][uresultser][createUser]: Joi Validate Result', result);

            if (result && result.hasOwnProperty('error') && result.error) {
                return res.status(400).json({
                    errors: [
                        { code: 'VALIDATION_ERROR', message: result.error.message }
                    ],
                });
            }

            
            let currentTime = moment().valueOf();
            let pass = await bcrypt.hash(payload.password, saltRounds);
            payload.password = pass;

            
            await knex.transaction(async trx => {
                let newId;
                let addedUser = await knex("users")
                    .insert({ ...payload, roleId: 3, createdBy: req.me.id, createdBy: req.me.id, createdAt: currentTime, updatedAt: currentTime })
                    .returning(["*"])
                    .transacting(trx);
                // addedUser = addedUser && addedUser[0] ? addedUser[0] : addedUser;
                // newId = addedUser?.id;
                // if(newId){
                //     let addedUserRole = await knex("user_roles")
                //     .insert({ userId : newId, roleId: 2, createdAt: currentTime, updatedAt: currentTime })
                //     .returning(["*"])
                //     .transacting(trx);
                // }
            });

            console.log('[controllers][v1][user][createUser]: Payload:', payload);

            return res.status(200).json({ data: {}, message: 'User Created Successfully.' });

        } catch (err) {
            console.log('[controllers][v1][user][createUser] :  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    updateUser: async (req, res) => {

        try {

            let id = req.params.id;
            if (!id) {
                res.status(400).json({
                    errors: [
                        { code: 'BAD_REQUEST', message: 'Invalid User Id' }
                    ],
                });
            }

            const payload = req.body;
            console.log('[controllers][v1][v1][user][updateUser]:', payload);

            const schema = Joi.object().keys({
                name: Joi.string().required().min(1).max(255),
                userName: Joi.string().required().min(1).max(255),
                email: Joi.string().required().min(1).max(500),
                mobileNo: Joi.string().required().min(1).max(20)
            });

            const result = schema.validate(payload);
            console.log('[controllers][v1][user][updateUser]: Joi Validate Result', result);

            if (result && result.hasOwnProperty('error') && result.error) {
                return res.status(400).json({
                    errors: [
                        { code: 'VALIDATION_ERROR', message: result.error.message }
                    ],
                });
            }

            let currentTime = moment().valueOf();

            await knex.transaction(async trx => {
                let updatedUsers = await knex("users")
                    .update({ ...payload, updatedBy:req.me.id, updatedAt: currentTime })
                    .where({id})
                    .returning(["*"])
                    .transacting(trx);

            });

            console.log('[controllers][v1][v1][user][updateUser]: Payload:', payload);

            return res.status(200).json({ data: {}, message: 'User Updated Successfully.' });

        } catch (err) {
            console.log('[controllers][v1][v1][user][updateUser]:  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },

    updateUserStatus: async (req, res) => {

        try {

            let id = req.params.id;
            if (!id) {
                res.status(400).json({
                    errors: [
                        { code: 'BAD_REQUEST', message: 'Invalid User Id' }
                    ],
                });
            }

            const payload = req.body;
            console.log('[controllers][v1][v1][user][updateUserStatus]:', payload);

            const schema = Joi.object().keys({
                isActive: Joi.boolean().required(),
            });

            const result = schema.validate(payload);
            console.log('[controllers][v1][user][updateUserStatus]: Joi Validate Result', result);

            if (result && result.hasOwnProperty('error') && result.error) {
                return res.status(400).json({
                    errors: [
                        { code: 'VALIDATION_ERROR', message: result.error.message }
                    ],
                });
            }

            let currentTime = moment().valueOf();

            await knex.transaction(async trx => {
                let updatedUsers = await knex("users")
                    .update({ ...payload, updatedBy:req.me.id, updatedAt: currentTime })
                    .where({id})
                    .returning(["*"])
                    .transacting(trx);

            });

            console.log('[controllers][v1][v1][user][updateUserStatus]: Payload:', payload);

            return res.status(200).json({ data: {}, message: 'User Status Updated Successfully.' });

        } catch (err) {
            console.log('[controllers][v1][v1][user][updateUserStatus]:  Error', err);
            res.status(500).json({
                errors: [
                    { code: 'UNKNOWN_SERVER_ERROR', message: err.message }
                ],
            });
        }
    },
};

module.exports = userController;